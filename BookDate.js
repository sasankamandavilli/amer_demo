import React from "react"
import { View, Text, StyleSheet, SafeAreaView, ScrollView, Pressable } from "react-native"
import * as LAYOUT from "./styles"
import Close from "./images/close.svg"
import { useState } from "react"

const BookingDate = ({ sendBookToParent }) => {
  const [book, setBook] = useState(false)
  return (
    <SafeAreaView style={{ backgroundColor: "#FFFFFF" }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.header}>
          <View></View>
          <Text style={styles.headerLabel}>Choose Date</Text>
          <Pressable
            onPress={() => {
              setBook(true)
              sendBookToParent(book)
            }}
          >
            <View>
              <Close size={15} />
            </View>
          </Pressable>
        </View>
        <View style={styles.line}></View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>10</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>SUN</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={[styles.selectDate, { backgroundColor: "#40C381", borderRadius: 10 }]}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>11</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>MON</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>12</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>TUE</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>13</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>WED</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>14</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>THU</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>15</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>FRI</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>16</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>SAT</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>17</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>SUN</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>18</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>MON</Text>
            </View>
          </View>
          <View style={styles.dateRow}>
            <View style={styles.selectDate}>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>May</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 24, fontFamily: "Roboto-Medium" }}>19</Text>
              <Text style={{ color: "#6A6A6A", fontSize: 12, fontFamily: "Roboto-Regular" }}>TUE</Text>
            </View>
          </View>
        </ScrollView>
        <View style={[styles.dateRow, { marginTop: 40, marginBottom: 20 }]}>
          <View style={styles.morningButton}>
            <Text style={{ color: "#FFFFFF", fontSize: 16, fontFamily: "Roboto-Regular", textAlign: "center" }}>Morning</Text>
          </View>
          <View style={[styles.morningButton, { backgroundColor: "#F4F4F4" }]}>
            <Text style={{ color: "#40C381", fontSize: 16, fontFamily: "Roboto-Regular", textAlign: "center" }}>AfterNoon</Text>
          </View>
          <View style={[styles.morningButton, { backgroundColor: "#F4F4F4" }]}>
            <Text style={{ color: "#40C381", fontSize: 16, fontFamily: "Roboto-Regular", textAlign: "center" }}>Evening</Text>
          </View>
        </View>
        <View style={[styles.dateRow, { marginVertical: 3 }]}>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
        </View>
        <View style={[styles.dateRow, { marginVertical: 3 }]}>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
        </View>
        <View style={[styles.dateRow, { marginVertical: 3 }]}>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
        </View>
        <View style={[styles.dateRow, { marginVertical: 3 }]}>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={[styles.dateButton, { borderColor: "#40C381" }]}>
            <Text style={{ color: "#40C381", fontSize: 12, fontFamily: "Roboto-Regular" }}>9:00AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
        </View>
        <View style={[styles.dateRow, { marginVertical: 3 }]}>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
        </View>
        <View style={[styles.dateRow, { marginVertical: 3 }]}>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
          <View style={styles.dateButton}>
            <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular" }}>8:30AM</Text>
          </View>
        </View>
        <Text style={{ color: "#000000", fontSize: 24, fontFamily: "Roboto-Medium", marginVertical: 40 }}>May 11 Monday, 09:00 Am</Text>
        <View style={styles.submitButton}>
          <Text style={{ color: "#FFFFFF", fontSize: 20, fontFamily: "Roboto-Medium" }}>Done</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default BookingDate
const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: 15
  },
  headerLabel: {
    fontSize: 16,
    fontFamily: "Roboto-Medium",
    fontWeight: "700"
  },
  line: {
    borderColor: "#F0F0F0",
    borderTopWidth: 1,
    width: LAYOUT.SCREEN_WIDTH
  },
  selectDate: {
    width: 71,
    height: 95,
    backgroundColor: "#FFFFFF",
    justifyContent: "center",
    alignItems: "center"
  },
  dateRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10
  },
  morningButton: {
    width: 118,
    height: 41,
    backgroundColor: "#40C381",
    borderRadius: 20,
    alignContent: "center",
    justifyContent: "center"
  },
  dateButton: {
    width: 70,
    height: 34,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#E2E2E2",
    borderRadius: 15
  },
  submitButton: {
    width: 364,
    height: 61,
    borderRadius: 30,
    backgroundColor: "#40C381",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20
  }
})
