
import {SWITCH_DARK_MODE} from "../constants/ActionTypes";
  
  export const switchDarkMode = (mode) => ({
    type: SWITCH_DARK_MODE,
    payload:mode
  });