
import {SWITCH_DIRECTION} from "../constants/ActionTypes";
  
export const switchDirection = (direction) => ({
  type: SWITCH_DIRECTION,
  payload:direction
});