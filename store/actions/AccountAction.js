import axios from "axios";
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { refreshAuthLogic } from '../../configs/Constants'
import { navigationRef } from '../../configs/RootNavigation';


import { GET_ACCOUNT_API_SUCCESS, GET_ACCOUNT_API_REQUEST, GET_ACCOUNT_API_FAILURE } from "../constants/ActionTypes";

export const accountRequest = () => ({
    type: GET_ACCOUNT_API_REQUEST
});

export const accountSuccess = json => ({
    type: GET_ACCOUNT_API_SUCCESS,
    payload: json
});

export const accountFailure = json => ({
    type: GET_ACCOUNT_API_FAILURE,
    payload: json
});

export const getAccountByUser = (url, token, langKey, userId) => {
    return async dispatch => {
        dispatch(accountRequest());
        let axy = axios.create()
        await createAuthRefreshInterceptor(axy, refreshAuthLogic, {
            statusCodes: [401, 403] // default: [ 401 ]
        }).then
            (axy({
                method: "get",
                url: url,
                headers: {
                    //  Authorization:`Bearer ${token}`,
                    'userid': userId,
                    'authorization': token,
                    'Accept-Language': langKey,
                },
                params: {
                    //  emirate: emirateId
                }
            })
                .then(response => {
                    if (response.data.isSucess) {
                        console.log('data------>', response.data)

                        dispatch(accountSuccess(response.data.response));
                    } else {
                        let error = langKey == 'en' ? response.data.msgEn : response.data.msgAr
                        dispatch(accountFailure(error));
                    }
                })
                .catch(error => dispatch(accountFailure('serverError'))));
    };
};