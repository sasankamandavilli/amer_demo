import axios from "axios";
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import { STORES_BY_EMIRATE_REQUEST, STORES_BY_EMIRATE_SUCCESS, STORES_BY_EMIRATE_FAILURE } from "../constants/ActionTypes";

export const storesByEmirateRequest = () => ({
    type: STORES_BY_EMIRATE_REQUEST
});

export const storesByEmirateSuccess = json => ({
    type: STORES_BY_EMIRATE_SUCCESS,
    payload: json
});

export const storesByEmirateFailure = json => ({
    type: STORES_BY_EMIRATE_FAILURE,
    payload: json
});

export const getStoresByEmirate = (url, token, langKey, emirateId) => {
    return async dispatch => {
        dispatch(storesByEmirateRequest());
        await axios({
            method: "get",
            url: url,
            headers: {
                Authorization: `Bearer ${token}`,
                'Accept-Language': langKey
            },
            params: {
                emirate: emirateId
            }
        })
            .then(response => {
                if (response.data.status === 1) {
                    dispatch(storesByEmirateSuccess(response.data.result));
                } else {
                    dispatch(storesByEmirateFailure(response.data.errorMessage));
                }
            })
            .catch(error => dispatch(storesByEmirateFailure('serverError')));
    };
};