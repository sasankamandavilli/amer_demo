import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import DarkModeReducer from './reducers/DarkModeReducer';
import DirectionReducer from './reducers/DirectionReducer';
import ApiNamesReducer from './reducers/ApiNamesReducer';
import AccountReducer from './reducers/AccountReducer';


const rootReducer = combineReducers({
  darkMode: DarkModeReducer,
  direction: DirectionReducer,
  apiService: ApiNamesReducer,
  accountService: AccountReducer,

});

const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;
