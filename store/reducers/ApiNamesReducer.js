const initialState = {
    data: {
        BASE_URL: 'http://94.206.47.14:6680/api/',
        //IMAGE_BASE_URL: 'http://94.206.47.14:6677/KFFDS/Images/',
        LOGINT: 'login/',
        REGISTER: "user/",
        FORGOTPASSWORD: "login/forgotPassword",
        GETUSERACCOUNT: "user/account",
        ADDACCOUNT: "account/",
        ADDINCOME: 'debit/',
        ADDEXPENSE: "credit/",
        GETINCOME: 'debit/findAll',
        GETEXPENSE: 'credit/findAll',
        GETINCOMECATEGORY: 'category/3',
        GETEXPENSECATEGORY: 'category/4',
        GETINVITEDACCOUNT: 'accountInvitation/',
        GETMYACCOUNTS: 'accountUsers/accounts/',
        SETDEFAULTACCOUNT: 'user/account',
        GETCATEGORY: 'category/',
        POSTCATEGORY: 'category/',
        UPDATEPROFILE: 'user/image',
        GETPROFILE: 'user/details/',
        CHANGEPASSWORD: 'user/changePassword',


    }
};

const ApiNamesReducer = (state = initialState) => {
    return state;
};

export default ApiNamesReducer;