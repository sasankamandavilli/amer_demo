import _ from 'lodash';
import {
    POST_REGISTER_REQUEST,
    POST_REGISTER_SUCCESS,
    POST_REGISTER_FAILURE,

} from '../constants/ActionTypes';
const initialState = {
    isFetching: false,
    errorMessage: null,
    data: [],
    postReviewData: false,
};

const REGISTERReducer = (state = initialState, action) => {
    switch (action.type) {
        // post rate review
        case POST_REGISTER_REQUEST:
            return {
                ...state,
                postReviewData: null,
                isFetching: true,
            };
        case POST_REGISTER_SUCCESS:
            //   state.data.push(action.payload[0]);
            return {
                ...state,
                postReviewData: action.payload,
                isFetching: false,
            };
        case POST_REGISTER_FAILURE:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.payload,
            };

        // get my review
        case GET_MY_REGISTER_REQUEST:
            return {
                ...state,
                isFetching: true,
                errorMessage: null,
                data: [],
            };
        case GET_MY_REGISTER_SUCCESS:
            return {
                ...state,
                isFetching: false,
                data: action.payload,
                errorMessage: null,
            };
        case GET_MY_REGISTER_FAILURE:
            return {
                ...state,
                isFetching: false,
                //  data: [],
                errorMessage: action.payload,
            };

        // delete rating review
        case DELETE__REGISTER_REQUEST:
            return {
                ...state,
            };
        case DELETE__REGISTER_SUCCESS:
            return {
                ...state,
            };
        case DELETE__REGISTER_FAILURE:
            return {
                ...state,
            };
        case UPDATE_REVIEW_SUBMIT_STATUS:
            return {
                ...state,
                postReviewData: action.payload
            };

        default:
            return state;
    }
};

export default REGISTERReducer;
