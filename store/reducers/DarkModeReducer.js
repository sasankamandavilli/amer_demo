import { SWITCH_DARK_MODE } from "../constants/ActionTypes";
  const initialState = {
    isFetching: true,
    errorMessage: null,
    data: 'light'
  };
  
  const DarkModeReducer = (state = initialState, action) => {
    switch (action.type) {
      case SWITCH_DARK_MODE:
        return { ...state, isFetching: false, data: action.payload};
    
      default:
        return state;
    }
  };
  export default DarkModeReducer;