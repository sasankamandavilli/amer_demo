import { STORES_BY_EMIRATE_REQUEST, STORES_BY_EMIRATE_SUCCESS, STORES_BY_EMIRATE_FAILURE } from "../constants/ActionTypes";
const initialState = {
    isFetching: false,
    errorMessage: null,
    data: [],
};

const StoresByEmirateReducer = (state = initialState, action) => {
    switch (action.type) {
        case STORES_BY_EMIRATE_REQUEST:
            return {
                ...state,
                isFetching: true,
                errorMessage: null,
                data: []
            };
        case STORES_BY_EMIRATE_SUCCESS:
            return {
                ...state,
                isFetching: false,
                data: [...action.payload],
                errorMessage: null,
            };
        case STORES_BY_EMIRATE_FAILURE:
            return {
                ...state,
                isFetching: false,
                data: [],
                errorMessage: action.payload
            };
        default:
            return state;
    }
};

export default StoresByEmirateReducer;