import { GET_ACCOUNT_API_SUCCESS, GET_ACCOUNT_API_REQUEST, GET_ACCOUNT_API_FAILURE } from "../constants/ActionTypes";
const initialState = {
    isFetching: false,
    errorMessage: null,
    data: [],
};

const AccountReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ACCOUNT_API_REQUEST:
            return {
                isFetching: true,
                errorMessage: null,
                data: []
            };
        case GET_ACCOUNT_API_SUCCESS:
            console.log(action.payload, 'payload------->')
            return {
                isFetching: false,
                data: [...action.payload],
                errorMessage: null,
            };
        case GET_ACCOUNT_API_FAILURE:
            return {
                isFetching: false,
                data: [],
                errorMessage: action.payload
            };
        default:
            return state;
    }
};

export default AccountReducer;