import { SWITCH_DIRECTION } from "../constants/ActionTypes";
  const initialState = {
    isFetching: true,
    errorMessage: null,
    data: 'ltr'
  };
  
  const SwitchDirectionReducer = (state = initialState, action) => {
    
    switch (action.type) {
      case SWITCH_DIRECTION:
        return { ...state, isFetching: false, data: action.payload};
    
      default:
        return state;
    }
  };
  export default SwitchDirectionReducer;