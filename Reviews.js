import * as React from "react"
import { View, Text, SafeAreaView, ScrollView, Pressable, StyleSheet } from "react-native"
import * as LAYOUT from "./styles"
import { Avatar } from "react-native-elements"
import Icon from "react-native-vector-icons/FontAwesome"
import Back from "./images/back2.svg"
import Star from "./images/star5.svg"
import One from "./images/star1.svg"

const Review = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[styles.header, { height: 88, justifyContent: "space-between" }]}>
          <Pressable onPress={() => navigation.goBack()}>
            <Back width={13} height={23} />
          </Pressable>
          <Text style={styles.headingLabel}>View All Reviews</Text>
          <View></View>
        </View>
        <View style={[styles.header, { height: 80, justifyContent: "flex-start" }]}>
          <Avatar size={36} source={require("./images/defaultlogo.png")} activeOpacity={0.7} />
          <View style={{ marginHorizontal: 10 }}>
            <Text style={{ fontSize: 16, fontFamily: "Roboto-Regular", color: "#000000" }}>First Track Government System LLC</Text>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start" }}>
              <Text style={{ color: "#666666", fontSize: 14, fontFamily: "Roboto-Regular", marginRight: 3 }}>Over all rating :</Text>
              <Star height={10} width={10} />
              <Text style={{ color: "#666666", fontSize: 14, fontFamily: "Roboto-Regular", marginHorizontal: 3 }}>3.5</Text>
            </View>
          </View>
          <View></View>
        </View>
        <View style={[styles.reviewCard]}>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 20 }}>
            <View>
              <Text style={{ color: "#666666", fontSize: 16, fontFamily: "Roboto-Medium" }}>364 Reviews</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Write")}>
              <View>
                <Text style={{ color: "#02B275", fontSize: 14, fontFamily: "Roboto-Regular", textDecorationLine: "underline" }}>Write a Review</Text>
              </View>
            </Pressable>
          </View>
          <View style={{ height: 0, backgroundColor: "#F0F0F0", borderTopWidth: 1, marginVertical: 10 }}></View>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "flex-start", justifyContent: "flex-start" }}>
              <Avatar size={57} source={require("./images/rev.png")} activeOpacity={0.7} />
              <View style={{ marginHorizontal: 10 }}>
                <Text style={{ color: "#CEB36F", fontSize: 16, fontFamily: "Roboto-Medium" }}>User Long name</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start", marginTop: 15 }}>
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                </View>
              </View>
            </View>
            <View>
              <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular" }}>20 May 2020</Text>
            </View>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start", marginTop: 15, paddingHorizontal: 5 }}>
            <Avatar height={72} width={90} marginRight={3} source={require("./images/review.png")} activeOpacity={0.7} />
            <Avatar height={72} width={90} marginHorizontal={3} source={require("./images/cent.png")} activeOpacity={0.7} />
            <Avatar height={72} width={90} marginHorizontal={3} source={require("./images/img3.png")} activeOpacity={0.7} />
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={{ color: "#505050", fontSize: 14, fontFamily: "Roboto-Regular" }}>Lorem Ipsum is simply dummy text of…Lorem Ipsum is simply dummy text of… Lorem Ipsum is simply dummy text of…Lorem Ipsum is simply dummy text of…</Text>
          </View>
          <View style={{ height: 0, backgroundColor: "#F0F0F0", borderTopWidth: 1, marginVertical: 10 }}></View>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "flex-start", justifyContent: "flex-start" }}>
              <Avatar size={57} source={require("./images/rev.png")} activeOpacity={0.7} />
              <View style={{ marginHorizontal: 10 }}>
                <Text style={{ color: "#CEB36F", fontSize: 16, fontFamily: "Roboto-Medium" }}>User Long name</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start", marginTop: 15 }}>
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                </View>
              </View>
            </View>
            <View>
              <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular" }}>20 May 2020</Text>
            </View>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start", marginTop: 15, paddingHorizontal: 5 }}>
            <Avatar height={72} width={90} marginRight={3} source={require("./images/review.png")} activeOpacity={0.7} />
            <Avatar height={72} width={90} marginHorizontal={3} source={require("./images/cent.png")} activeOpacity={0.7} />
            <Avatar height={72} width={90} marginHorizontal={3} source={require("./images/img3.png")} activeOpacity={0.7} />
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={{ color: "#505050", fontSize: 14, fontFamily: "Roboto-Regular" }}>Lorem Ipsum is simply dummy text of…Lorem Ipsum is simply dummy text of… Lorem Ipsum is simply dummy text of…Lorem Ipsum is simply dummy text of…</Text>
          </View>
          <View style={{ height: 0, backgroundColor: "#F0F0F0", borderTopWidth: 1, marginVertical: 10 }}></View>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "flex-start", justifyContent: "flex-start" }}>
              <Avatar size={57} source={require("./images/rev.png")} activeOpacity={0.7} />
              <View style={{ marginHorizontal: 10 }}>
                <Text style={{ color: "#CEB36F", fontSize: 16, fontFamily: "Roboto-Medium" }}>User Long name</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start", marginTop: 15 }}>
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                  <One height={15} width={15} style={{ marginHorizontal: 3 }} />
                </View>
              </View>
            </View>
            <View>
              <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular" }}>20 May 2020</Text>
            </View>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start", marginTop: 15, paddingHorizontal: 5 }}>
            <Avatar height={72} width={90} marginRight={3} source={require("./images/review.png")} activeOpacity={0.7} />
            <Avatar height={72} width={90} marginHorizontal={3} source={require("./images/cent.png")} activeOpacity={0.7} />
            <Avatar height={72} width={90} marginHorizontal={3} source={require("./images/img3.png")} activeOpacity={0.7} />
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={{ color: "#505050", fontSize: 14, fontFamily: "Roboto-Regular" }}>Lorem Ipsum is simply dummy text of…Lorem Ipsum is simply dummy text of… Lorem Ipsum is simply dummy text of…Lorem Ipsum is simply dummy text of…</Text>
          </View>
          <View style={{ height: 0, backgroundColor: "#F0F0F0", borderTopWidth: 1, marginVertical: 10 }}></View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default Review

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    backgroundColor: "#FFFFFF",
    width: LAYOUT.SCREEN_WIDTH,
    marginBottom: 10
  },
  headingLabel: {
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    fontWeight: "600"
  },
  reviewCard: {
    paddingHorizontal: 20,
    backgroundColor: "#FFFFFF",
    width: LAYOUT.SCREEN_WIDTH,
    marginBottom: 10
  }
})
