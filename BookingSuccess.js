import * as React from "react"
import { View, Text, SafeAreaView, StyleSheet, ScrollView, TextInput, Pressable } from "react-native"
import Happy from "./images/happy.svg"
import SuccessLogo from "./images/success.svg"
import Dash from "./images/dash.svg"
import Nuetral from "./images/nutral.svg"
import UnHappy from "./images/unhappy.svg"
import * as LAYOUT from "./styles"

const BookingSuccess = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.successCard}>
          <SuccessLogo size={81} />
          <Text style={{ fontSize: 21, fontFamily: "Roboto-Medium", fontWeight: "600", color: "#40C381" }}>Booking Successful</Text>
          <Text style={{ fontSize: 23, fontFamily: "Roboto-Medium", fontWeight: "600", color: "#000000" }}>Token Number</Text>
          <Text style={{ fontSize: 36, fontFamily: "Roboto-Bold", fontWeight: "bold", color: "#000000" }}>Q 19001</Text>
          <Text style={{ fontSize: 20, fontFamily: "Roboto-Medium", fontWeight: "600", color: "#000000" }}>May 11 Monday, 08:00 Am</Text>
        </View>
        <View>
          <Text style={{ fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600", color: "#40C381", textAlign: "center" }}>Share your experience</Text>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginVertical: 15 }}>
            <Happy size={81} />
            <Nuetral size={81} />
            <UnHappy size={81} />
          </View>
          <TextInput style={styles.input} placeholder="Help us to improve our service"></TextInput>
          <View style={{ flexDirection: "row-reverse" }}>
            <View style={styles.sendButton}>
              <Text style={{ fontSize: 14, fontFamily: "Roboto-Medium", fontWeight: "600", color: "#FFFFFF", textAlign: "center" }}>Send</Text>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={styles.appointmentButton}>
        <Pressable onPress={() => navigation.navigate("Home")}>
          <View style={{ width: 59, height: 59, backgroundColor: "#29AA69", borderRadius: 59, justifyContent: "center", alignItems: "center" }}>
            <Dash size={23} />
          </View>
        </Pressable>
        <Pressable onPress={() => navigation.navigate("Bookings")}>
          <View style={{ width: 309, height: 59, backgroundColor: "#29AA69", borderRadius: 59, justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600", color: "#FFFFFF", textAlign: "center" }}>View Bookings</Text>
          </View>
        </Pressable>
      </View>
    </SafeAreaView>
  )
}
export default BookingSuccess

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingHorizontal: 40,
    backgroundColor: "#FFFFFF"
  },
  successCard: {
    marginTop: 35,
    height: 364,
    width: 324,
    borderColor: "#40C381",
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "space-evenly",
    borderWidth: 1,
    paddingVertical: 20,
    marginBottom: 20
  },
  input: {
    height: 109,
    width: 327,
    borderColor: "#BEBEBE",
    borderRadius: 20,
    color: "#888888",
    fontSize: 16,
    borderWidth: 1,
    padding: 20,
    marginVertical: 20
  },
  sendButton: {
    width: 91,
    height: 36,
    borderRadius: 15,
    backgroundColor: "#40C381",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 80
  },
  appointmentButton: {
    position: "absolute",
    bottom: 0,
    right: 0,
    width: LAYOUT.SCREEN_WIDTH,
    height: 70,
    backgroundColor: "#40C381",
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center"
  }
})
