import "react-native-gesture-handler"
import * as React from "react"
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import HomeScreen from "./HomeScreen"
import Services from "./Services"
import { Avatar } from "react-native-elements"
import Centers from "./Centers"
import Settings from "./Settings"
import Bookings from "./MyBookings"
import Contacts from "./ContactUs"
import Profile from "./Profile"
import BookingDetails from "./BookingDetail"
import ServiceBooking from "./serviceBooking"
import RenewService from "./RenewService"
import Appointment from "./Appointment"
import AppointmentService from "./AppointmentService"
import Upload from "./upload"
import LanguagePicker from "./LanguagePicker"
import Library from "./Library"
import Review from "./Reviews"
import WriteReview from "./WriteReview"
import { useSelector, useDispatch, Provider } from "react-redux"
import store from "./store/configureStore"
import { switchDarkMode } from "./store/actions/DarkModeAction"
import Strings from "./utilities/i18nStrings"
import BookingSuccess from "./BookingSuccess"

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

function ReviewStack() {
  return (
    <Stack.Navigator
      initialRouteName="Appointment"
      screenOptions={{
        headerShown: false
      }}
    >
      <Stack.Screen name="Appointment" component={Appointment} />
      <Stack.Screen name="Review" component={Review} />
      <Stack.Screen name="Write" component={WriteReview} />
    </Stack.Navigator>
  )
}

function BookStack() {
  return (
    <Stack.Navigator
      initialRouteName="BookingDetails"
      screenOptions={{
        headerShown: false
      }}
    >
      <Stack.Screen name="Details" component={BookingDetails} />
    </Stack.Navigator>
  )
}
function ServiceStack() {
  return (
    <Stack.Navigator
      initialRouteName="Service"
      screenOptions={{
        headerShown: false
      }}
    >
      <Stack.Screen name="Renew" component={RenewService} />
      <Stack.Screen name="ServiceBooking" component={ServiceBooking} />
      <Stack.Screen name="Success" component={BookingSuccess} />
    </Stack.Navigator>
  )
}
function CenterStack() {
  return (
    <Stack.Navigator
      initialRouteName="AppointmentService"
      screenOptions={{
        headerShown: false
      }}
    >
      <Stack.Screen name="ReviewStack" component={ReviewStack} />
      <Stack.Screen name="AppointmentService" component={AppointmentService} />
      <Stack.Screen name="ServiceBooking" component={ServiceBooking} />
    </Stack.Navigator>
  )
}
function SettingStack() {
  return (
    <Stack.Navigator
      initialRouteName="Library"
      screenOptions={{
        headerShown: false
      }}
    >
      <Stack.Screen name="Upload" component={Upload} />
      <Stack.Screen name="Library" component={Library} />
      <Stack.Screen name="LanguagePicker" component={LanguagePicker} />
    </Stack.Navigator>
  )
}
function MyHomeTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Feed"
      screenOptions={{ headerShown: false }}
      tabBarOptions={{
        activeTintColor: "#40C381"
      }}
    >
      <Tab.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          tabBarLabel: "Dashboard",
          tabBarIcon: () => <Avatar size={20} source={require("./images/dashboard.png")} activeOpacity={0.7} />
        }}
      />
      <Tab.Screen
        name="Centers"
        component={Centers}
        options={{
          tabBarLabel: "Centers",
          tabBarIcon: () => <Avatar size={20} source={require("./images/centers.png")} activeOpacity={0.7} />
        }}
      />
      <Tab.Screen
        name="Services"
        component={Services}
        options={{
          tabBarLabel: "Services",
          tabBarIcon: () => <Avatar size={20} source={require("./images/services.png")} activeOpacity={0.7} />
        }}
      />
      <Tab.Screen
        name="Bookings"
        component={Bookings}
        options={{
          tabBarLabel: "Bookings",
          tabBarIcon: () => <Avatar size={20} source={require("./images/bookings.png")} activeOpacity={0.7} />
        }}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={{
          tabBarLabel: "Settings",
          tabBarIcon: () => <Avatar size={20} source={require("./images/setting.png")} activeOpacity={0.7} />
        }}
      />
    </Tab.Navigator>
  )
}

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="MyHomeTabs"
          screenOptions={{
            headerShown: false
          }}
        >
          <Stack.Screen name="MyHome" component={MyHomeTabs} />
          <Stack.Screen name="SettingStack" component={SettingStack} />
          <Stack.Screen name="ServiceStack" component={ServiceStack} />
          <Stack.Screen name="CenterStack" component={CenterStack} />
          <Stack.Screen name="ReviewStack" component={ReviewStack} />
          <Stack.Screen name="BookStack" component={BookStack} />
          <Stack.Screen name="Contacts" component={Contacts} />
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen name="LanguagePicker" component={LanguagePicker} />
          <Stack.Screen name="Success" component={BookingSuccess} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}
export default App
