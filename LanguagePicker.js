// React Native Bottom Navigation
// https://aboutreact.com/react-native-bottom-navigation/

import * as React from "react"
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Pressable } from "react-native"
import Check from "./images/check.svg"
import Back from "./images/back2.svg"
import Angle from "./images/angle.svg"
import Icon from "react-native-vector-icons/MaterialIcons"

const LanguagePicker = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <ScrollView>
        <View style={styles.header}>
          <Pressable onPress={() => navigation.goBack()}>
            <Back height={22} width={12} />
          </Pressable>
          <Text style={styles.heading}>Language</Text>
          <View></View>
        </View>
        <View style={[styles.profileCard, { height: 284 }]}>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Arabic</Text>
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>English</Text>
            </View>
            <View style={{ paddingRight: 20 }}>
              <Icon name="check" size={18} color={"#40C381"} />
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Urdu</Text>
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Chinnese</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default LanguagePicker

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
    marginVertical: 10
  },
  heading: {
    fontSize: 18,
    fontFamily: "Roboto-Medium",
    fontWeight: "800"
  },
  profileCard: {
    width: 374,
    marginVertical: 10,
    backgroundColor: "#FFFFFF",
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#F5F5F5"
  },
  cardInside: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingVertical: 20
  },
  font: {
    fontFamily: "Segoe-UI Bold",
    fontWeight: "800"
  }
})
