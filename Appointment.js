import * as React from "react"
import { View, Text, SafeAreaView, Pressable, StyleSheet, ScrollView, ImageBackground } from "react-native"
import * as LAYOUT from "./styles"
import { Avatar } from "react-native-elements"
import Icon from "react-native-vector-icons/FontAwesome"
import Share from "./images/shareDark.svg"
import Fill from "./images/locationfill.svg"
import Star from "./images/star.svg"
import Back from "./images/backDark.svg"
import Heart from "./images/heart.svg"
import Distance from "./images/distance.svg"
import Email from "./images/mail.svg"
import Phone from "./images/phoneblack.svg"
import Five from "./images/star5.svg"
import Four from "./images/star4.svg"
import Three from "./images/star3.svg"
import Two from "./images/star2.svg"
import One from "./images/star1.svg"
import AppointmentLogo from "./images/appointment.svg"
import EmailWhite from "./images/mailwhite.svg"
import PhoneWhite from "./images/phonewhite.svg"

const imageC = require("D:/yash/reactnative/project/reactNativeAppSample/images/aptbg.png")
const imageBack = require("D:/yash/reactnative/project/reactNativeAppSample/images/aptbgt.png")

const Appointment = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <ImageBackground source={imageC} style={styles.image}>
            <ImageBackground source={imageBack} style={styles.imageAmer}>
              <View style={styles.header}>
                <Pressable onPress={() => navigation.goBack()}>
                  <Back height={40} width={40} />
                </Pressable>
                <View></View>
                <Share height={40} width={40} />
              </View>
              <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 75, paddingBottom: 20, paddingHorizontal: 20, marginBottom: 10 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                  <View style={{ width: 57, height: 26, borderRadius: 10, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                      <Star height={10} width={10}></Star>
                      <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>4.5</Text>
                    </View>
                  </View>
                  <View style={{ marginVertical: 3, marginLeft: 5 }}>
                    <Text style={{ color: "#FFFFFF", fontSize: 14 }}>Very Good (30 Reviews)</Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginTop: 5 }}>
                  <Heart height={12} width={12} Fill={"#40C381"} />
                  <Text style={{ color: "#40C381", fontSize: 13, marginHorizontal: 3 }}>1/5</Text>
                </View>
              </View>
              <View style={styles.body}>
                <View style={{ flexDirection: "row", marginHorizontal: 20, alignItems: "center", marginTop: 30 }}>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>OPEN</Text>
                  <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", marginHorizontal: 20 }}></View>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal" }}>Working Hours - 08:00 Am to 02:30 Pm</Text>
                </View>
              </View>
            </ImageBackground>
          </ImageBackground>
          <View style={{ width: LAYOUT.SCREEN_WIDTH, backgroundColor: "#FFFFFF", height: LAYOUT.SCREEN_HEIGHT + 280 }}>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start", paddingHorizontal: 20, marginTop: 10 }}>
              <Avatar size={65} source={require("./images/defaultlogo.png")} activeOpacity={0.7} />
              <Text style={{ paddingHorizontal: 10, fontSize: 20, fontFamily: "Roboto-Bold", fontWeight: "bold" }}>First Track Government System {"\n"}LLC</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 20, marginBottom: 10, alignItems: "center", marginTop: 10 }}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <Fill />
                <View style={{ paddingHorizontal: 10 }}>
                  <Text style={{ fontSize: 14, color: "#080F0B" }}>Al Waha Street, Al Quoz 1, Behind {"\n"}Oasis center, Al quiz</Text>
                </View>
              </View>

              <View style={{ width: 77, height: 26, borderRadius: 20, backgroundColor: "#40C381" }}>
                <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginTop: 5 }}>
                  <Distance height={14.75} width={14.75}></Distance>
                  <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>2 KM</Text>
                </View>
              </View>
            </View>
            <View style={{ margin: 40, backgroundColor: "#F4F4F4" }}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
                <View>
                  <Text style={{ color: "#5A5A5A", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Phone Number</Text>
                  <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>+971 50655774</Text>
                </View>
                <View>
                  <Phone height={26} width={26} />
                </View>
              </View>
              <View style={{ height: 0, borderColor: "#F0F0F0", borderTopWidth: 1, marginVertical: 10, marginHorizontal: 10 }}></View>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
                <View>
                  <Text style={{ color: "#5A5A5A", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Email</Text>
                  <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>info@smartnts@gmail.com</Text>
                </View>
                <View>
                  <Email height={26} width={26} fill={"#0B0B0B"} />
                </View>
              </View>
            </View>
            <Pressable onPress={() => navigation.navigate("AppointmentService")}>
              <View style={styles.serviceButton}>
                <Text style={{ color: "#17C667", fontSize: 18, fontFamily: "Roboto-Regular" }}>View All Services</Text>
              </View>
            </Pressable>
            <View style={{ marginTop: 30, marginHorizontal: 20 }}>
              <Text style={{ color: "#000000", fontSize: 22, fontFamily: "Roboto-Bold", fontWeight: "bold" }}>Description</Text>
              <Text style={{ color: "#000000", fontSize: 16, fontFamily: "Roboto-Regular", fontWeight: "normal", marginTop: 10 }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 30, paddingBottom: 20, paddingHorizontal: 20 }}>
              <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <View style={{ width: 57, height: 26, borderRadius: 10, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                  <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                    <Star height={10} width={10}></Star>
                    <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>4.5</Text>
                  </View>
                </View>
                <Pressable onPress={() => navigation.navigate("Review")}>
                  <View style={{ marginVertical: 3, marginLeft: 5 }}>
                    <Text style={{ color: "#1EC667", fontSize: 14, textDecorationLine: "underline", fontFamily: "Roboto-Bold" }}>36 Reviews</Text>
                  </View>
                </Pressable>
              </View>
              <Pressable onPress={() => navigation.navigate("Write")}>
                <View>
                  <Text style={{ color: "#17C667", fontSize: 13, marginHorizontal: 3, textDecorationLine: "underline" }}>Write a Review</Text>
                </View>
              </Pressable>
            </View>
            <View style={{ flexDirection: "row-reverse", justifyContent: "flex-start", alignItems: "center", marginLeft: 20 }}>
              <Text style={{ paddingHorizontal: 20 }}>40%</Text>
              <View style={{ width: 199, height: 6, backgroundColor: "#F1F1F1", borderRadius: 3 }}>
                <View style={{ width: 160, height: 6, backgroundColor: "#018123", borderRadius: 3 }}></View>
              </View>
              <Five height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10, marginRight: 5 }} />
              <Five height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
              <Five height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
              <Five height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
              <Five height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
            </View>
            <View style={{ flexDirection: "row-reverse", justifyContent: "flex-start", alignItems: "center", marginHorizontal: 20, marginTop: 20 }}>
              <Text style={{ paddingHorizontal: 20 }}>20%</Text>
              <View style={{ width: 199, height: 6, backgroundColor: "#F1F1F1", borderRadius: 3 }}>
                <View style={{ width: 100, height: 6, backgroundColor: "#00A200", borderRadius: 3 }}></View>
              </View>
              <Four height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10, marginRight: 5 }} />
              <Four height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
              <Four height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
              <Four height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
            </View>
            <View style={{ flexDirection: "row-reverse", justifyContent: "flex-start", alignItems: "center", marginHorizontal: 20, marginTop: 20 }}>
              <Text style={{ paddingHorizontal: 20 }}>30%</Text>
              <View style={{ width: 199, height: 6, backgroundColor: "#F1F1F1", borderRadius: 3 }}>
                <View style={{ width: 120, height: 6, backgroundColor: "#93B502", borderRadius: 3 }}></View>
              </View>
              <Three height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10, marginRight: 5 }} />
              <Three height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
              <Three height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
            </View>
            <View style={{ flexDirection: "row-reverse", justifyContent: "flex-start", alignItems: "center", marginHorizontal: 20, marginTop: 20 }}>
              <Text style={{ paddingHorizontal: 20 }}>24%</Text>
              <View style={{ width: 199, height: 6, backgroundColor: "#F1F1F1", borderRadius: 3 }}>
                <View style={{ width: 60, height: 6, backgroundColor: "#FFD639", borderRadius: 3 }}></View>
              </View>
              <Two height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10, marginRight: 5 }} />
              <Two height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 10 }} />
            </View>
            <View style={{ flexDirection: "row-reverse", justifyContent: "flex-start", alignItems: "center", marginHorizontal: 20, marginTop: 20 }}>
              <Text style={{ paddingHorizontal: 20 }}>12%</Text>
              <View style={{ width: 199, height: 6, backgroundColor: "#F1F1F1", borderRadius: 3 }}>
                <View style={{ width: 20, height: 6, backgroundColor: "#FFA200", borderRadius: 3 }}></View>
              </View>
              <One height={10} width={10} fill={"#018123"} style={{ paddingHorizontal: 15 }} />
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={styles.appointmentButton}>
        <View style={{ width: 59, height: 59, backgroundColor: "#29AA69", borderRadius: 59, justifyContent: "center", alignItems: "center" }}>
          <EmailWhite height={20} width={26} fill={"#FFFFFF"} />
        </View>
        <View style={{ width: 59, height: 59, backgroundColor: "#29AA69", borderRadius: 59, justifyContent: "center", alignItems: "center" }}>
          <PhoneWhite height={24} width={24} />
        </View>
        <Pressable onPress={() => navigation.navigate("CenterStack")}>
          <View style={{ width: 236, height: 59, backgroundColor: "#29AA69", borderRadius: 30, justifyContent: "center", alignItems: "center" }}>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
              <AppointmentLogo height={27} width={27} />
              <Text style={{ fontSize: 18, color: "#FFFFFF", fontFamily: "Roboto-Regular", fontWeight: "normal", paddingHorizontal: 5 }}>Book Appointment</Text>
            </View>
          </View>
        </Pressable>
      </View>
    </SafeAreaView>
  )
}
export default Appointment
const styles = StyleSheet.create({
  body: {
    width: LAYOUT.SCREEN_WIDTH,
    height: LAYOUT.SCREEN_HEIGHT,
    backgroundColor: "#FFFFFF",
    borderTopEndRadius: 40,
    borderTopStartRadius: 40
  },
  image: {
    width: LAYOUT.SCREEN_WIDTH,
    height: 267
  },
  imageAmer: {
    width: LAYOUT.SCREEN_WIDTH,
    height: 267
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 30,
    paddingHorizontal: 20,
    marginTop: 20
  },
  appointmentButton: {
    position: "absolute",
    bottom: 0,
    right: 0,
    width: LAYOUT.SCREEN_WIDTH,
    height: 70,
    backgroundColor: "#40C381",
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  serviceButton: {
    marginHorizontal: 20,
    width: 374,
    height: 57,
    borderRadius: 30,
    borderColor: "#1EC667",
    borderWidth: 1,
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center"
  }
})
