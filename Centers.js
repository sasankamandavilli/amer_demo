import React from "react"
import { View, Text, StyleSheet, TextInput, Image, SafeAreaView, ScrollView, ImageBackground, Pressable, Picker } from "react-native"
import * as LAYOUT from "./styles"
import { Avatar } from "react-native-elements"
import Icon from "react-native-vector-icons/FontAwesome"
import Filter from "./images/filter.svg"
import MapView from "./images/mapview.svg"
import Back from "./images/back.svg"
import Center from "./images/center.svg"
import Star from "./images/star.svg"
import Distance from "./images/distance.svg"
import Call from "./images/call.svg"
import Email from "./images/email.svg"

const imageTwo = require("D:/yash/reactnative/project/reactNativeAppSample/images/cent.png")
const imageC = require("D:/yash/reactnative/project/reactNativeAppSample/images/centerC.png")
const imageBack = require("D:/yash/reactnative/project/reactNativeAppSample/images/logoback.png")

const Centers = ({ navigation }) => {
  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.card}>
            <View style={styles.cardInside}>
              <View>
                <Text style={[styles.normal, { color: "#A7A7A7" }]}>Location</Text>
                <View style={styles.location}>
                  <Text style={styles.bigBlue}>Mirdif</Text>
                  <View style={{ padding: 10 }}>
                    <Back width={15} height={9} fill={"#40C381"} />
                  </View>
                </View>
              </View>
              <View style={{ padding: 10 }}>
                <Avatar size={32} source={require("./images/face.png")} activeOpacity={0.7} />
              </View>
            </View>
            <View style={styles.searchInputContainer}>
              <Icon name="search" size={17} color={"#A7A7A7"} />
              <TextInput style={{ paddingHorizontal: 8 }} placeholder="I want" />
            </View>
          </View>
          <View style={styles.buttonCard}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <View style={[styles.button, { marginLeft: 20 }]}>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 8 }}>
                  <Filter height={18} width={18} />
                  <Text style={{ fontSize: 14, fontFamily: "Segoe UI", fontWeight: "900", paddingHorizontal: 4 }}>Filter</Text>
                </View>
              </View>
              <View style={styles.button}>
                <View style={{ alignItems: "center", marginTop: 8 }}>
                  <Text style={{ fontSize: 14, fontFamily: "Segoe UI", fontWeight: "900", paddingHorizontal: 4 }}>Near to Me</Text>
                </View>
              </View>
              <View style={styles.button}>
                <View style={{ alignItems: "center", marginTop: 8 }}>
                  <Text style={{ fontSize: 14, fontFamily: "Segoe UI", fontWeight: "900", paddingHorizontal: 4 }}>Top Rated</Text>
                </View>
              </View>
              <View style={styles.button}>
                <View style={{ alignItems: "center", marginTop: 8 }}>
                  <Text style={{ fontSize: 14, fontFamily: "Segoe UI", fontWeight: "900", paddingHorizontal: 4 }}>Waiting Time</Text>
                </View>
              </View>
              <View style={styles.button}>
                <View style={{ alignItems: "center", marginTop: 8 }}>
                  <Text style={{ fontSize: 14, fontFamily: "Segoe UI", fontWeight: "900", paddingHorizontal: 4 }}>Others</Text>
                </View>
              </View>
            </ScrollView>
          </View>
          <Pressable
            onPress={() => {
              navigation.navigate("ReviewStack")
            }}
          >
            <ImageBackground source={imageTwo} style={styles.image}>
              <ImageBackground style={{ width: 384, height: 142 }} source={imageC}>
                <View style={{ marginLeft: 28, marginTop: 19 }}>
                  <ImageBackground source={imageBack} style={{ height: 42, width: 42 }}>
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                      <Avatar size={38} source={require("./images/default.png")} activeOpacity={0.7} />
                    </View>
                  </ImageBackground>

                  <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 45 }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                      <View style={{ width: 57, height: 26, borderRadius: 10, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                        <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                          <Star height={10} width={10}></Star>
                          <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>4.5</Text>
                        </View>
                      </View>
                      <View style={{ marginVertical: 3, marginHorizontal: 5 }}>
                        <Text style={{ color: "#FFFFFF", fontSize: 14 }}>Very Good (30 Reviews)</Text>
                      </View>
                    </View>
                    <View style={{ width: 77, height: 26, borderRadius: 20, backgroundColor: "#40C381", marginRight: 10 }}>
                      <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginTop: 5 }}>
                        <Distance height={14.75} width={14.75}></Distance>
                        <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>2 KM</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </ImageBackground>
            </ImageBackground>
          </Pressable>
          <View style={styles.imageCard}>
            <View style={{ flexDirection: "row", marginHorizontal: 20, alignItems: "center", marginHorizontal: 10, marginTop: 10 }}>
              <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>OPEN</Text>
              <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", marginHorizontal: 20 }}></View>
              <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal" }}>Working Hours - 08:00 Am to 02:30 Pm</Text>
            </View>
            <View style={{ margin: 12 }}>
              <Text style={{ fontSize: 18, fontWeight: "bold", fontFamily: "Segoe UI" }}>First Track Government Transaction</Text>
            </View>
            <View style={{ marginBottom: 12, marginHorizontal: 12 }}>
              <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe UI" }}>Al What Street, Al Quoz 1, Behind Oasis center…</Text>
            </View>
            <View style={{ flexDirection: "row", marginHorizontal: 20, alignItems: "center", marginHorizontal: 10 }}>
              <Call height={15} width={15} />
              <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", marginLeft: 10 }}>+971 23456789</Text>
              <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", marginHorizontal: 20 }}></View>
              <Email height={15} width={15} />
              <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", marginLeft: 10 }}>info@firsttrack.ae</Text>
            </View>
          </View>
          <ImageBackground source={imageTwo} style={styles.image}>
            <ImageBackground style={{ width: 384, height: 142 }} source={imageC}>
              <View style={{ marginLeft: 28, marginTop: 19 }}>
                <ImageBackground source={imageBack} style={{ height: 42, width: 42 }}>
                  <View style={{ justifyContent: "center", alignItems: "center" }}>
                    <Avatar size={38} source={require("./images/default.png")} activeOpacity={0.7} />
                  </View>
                </ImageBackground>

                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 45 }}>
                  <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <View style={{ width: 57, height: 26, borderRadius: 10, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                      <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                        <Star height={10} width={10}></Star>
                        <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>4.5</Text>
                      </View>
                    </View>
                    <View style={{ marginVertical: 3, marginLeft: 5 }}>
                      <Text style={{ color: "#FFFFFF", fontSize: 14 }}>Very Good (30 Reviews)</Text>
                    </View>
                  </View>
                  <View style={{ width: 77, height: 26, borderRadius: 20, backgroundColor: "#40C381", marginRight: 10 }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginTop: 5 }}>
                      <Distance height={14.75} width={14.75}></Distance>
                      <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>2 KM</Text>
                    </View>
                  </View>
                </View>
              </View>
            </ImageBackground>
          </ImageBackground>
          <View style={styles.imageCard}>
            <View style={{ flexDirection: "row", marginHorizontal: 20, alignItems: "center", marginHorizontal: 10, marginTop: 10 }}>
              <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>OPEN</Text>
              <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", marginHorizontal: 20 }}></View>
              <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal" }}>Working Hours - 08:00 Am to 02:30 Pm</Text>
            </View>
            <View style={{ margin: 12, color: "#000000", fontSize: 18, fontWeight: "900", fontFamily: "Segoe UI" }}>
              <Text style={{ fontSize: 18, fontWeight: "bold", fontFamily: "Segoe UI" }}>First Track Government Transaction</Text>
            </View>
            <View style={{ marginBottom: 12, marginHorizontal: 12 }}>
              <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe UI" }}>Al What Street, Al Quoz 1, Behind Oasis center…</Text>
            </View>
            <View style={{ flexDirection: "row", marginHorizontal: 20, alignItems: "center", marginHorizontal: 10 }}>
              <Call height={15} width={15} />
              <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", marginLeft: 10 }}>+971 23456789</Text>
              <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", marginHorizontal: 20 }}></View>
              <Email height={15} width={15} />
              <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", marginLeft: 10 }}>info@firsttrack.ae</Text>
            </View>
          </View>
          <ImageBackground source={imageTwo} style={styles.image}>
            <ImageBackground style={{ width: 384, height: 142 }} source={imageC}>
              <View style={{ marginLeft: 28, marginTop: 19 }}>
                <ImageBackground source={imageBack} style={{ height: 42, width: 42 }}>
                  <View style={{ justifyContent: "center", alignItems: "center" }}>
                    <Avatar size={38} source={require("./images/default.png")} activeOpacity={0.7} />
                  </View>
                </ImageBackground>

                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 45 }}>
                  <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <View style={{ width: 57, height: 26, borderRadius: 10, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                      <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                        <Star height={10} width={10}></Star>
                        <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>4.5</Text>
                      </View>
                    </View>
                    <View style={{ marginVertical: 3, marginLeft: 5 }}>
                      <Text style={{ color: "#FFFFFF", fontSize: 14 }}>Very Good (30 Reviews)</Text>
                    </View>
                  </View>
                  <View style={{ width: 77, height: 26, borderRadius: 20, backgroundColor: "#40C381", marginRight: 10 }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginTop: 5 }}>
                      <Distance height={14.75} width={14.75}></Distance>
                      <Text style={{ color: "#FFFFFF", fontSize: 13, marginHorizontal: 3 }}>2 KM</Text>
                    </View>
                  </View>
                </View>
              </View>
            </ImageBackground>
          </ImageBackground>
          <View style={styles.imageCard}>
            <View style={{ flexDirection: "row", marginHorizontal: 20, alignItems: "center", marginHorizontal: 10, marginTop: 10 }}>
              <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>OPEN</Text>
              <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", marginHorizontal: 20 }}></View>
              <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal" }}>Working Hours - 08:00 Am to 02:30 Pm</Text>
            </View>
            <View style={{ margin: 12 }}>
              <Text style={{ fontSize: 18, fontWeight: "bold", fontFamily: "Segoe UI" }}>First Track Government Transaction</Text>
            </View>
            <View style={{ marginBottom: 12, marginHorizontal: 12 }}>
              <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe UI" }}>Al What Street, Al Quoz 1, Behind Oasis center…</Text>
            </View>
            <View style={{ flexDirection: "row", marginHorizontal: 20, alignItems: "center", marginHorizontal: 10 }}>
              <Call height={15} width={15} />
              <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", marginLeft: 10 }}>+971 23456789</Text>
              <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", marginHorizontal: 20 }}></View>
              <Email height={15} width={15} />
              <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", marginLeft: 10 }}>info@firsttrack.ae</Text>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={{ position: "absolute", bottom: 9, right: 140 }}>
        <View style={{ width: 122, height: 39, borderRadius: 20, backgroundColor: "#40C381", marginRight: 10, justifyContent: "center", alignItems: "center" }}>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
            <MapView height={13} width={13}></MapView>
            <Text style={{ color: "#FFFFFF", fontSize: 14, marginHorizontal: 7, fontFamily: "Helvetica Neue", fontWeight: "normal" }}>Map View</Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  )
}

export default Centers

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  buttonCard: {
    height: 60,
    width: LAYOUT.SCREEN_WIDTH,
    backgroundColor: "#E4E4E4",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  button: {
    height: 35,
    width: 95,
    borderRadius: 20,
    backgroundColor: "#EFEFF6",
    borderWidth: 1,
    borderColor: "#F5F5F5",
    marginHorizontal: 5
  },
  bigBlue: {
    color: "#40C381",
    fontWeight: "bold",
    fontSize: 22
  },
  normal: {
    color: "black",
    fontSize: 12
  },
  card: {
    width: LAYOUT.SCREEN_WIDTH,
    height: 130,
    backgroundColor: "#FFFFFF",
    padding: 10,
    borderColor: "red",
    flexDirection: "column",
    paddingHorizontal: 20
  },
  cardInside: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  left: {
    flexDirection: "row-reverse"
  },
  location: {
    flexDirection: "row"
  },
  searchInputContainer: {
    height: 36,
    backgroundColor: "#EFEFF4",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    borderRadius: 12
  },
  tinyLogo: {
    width: 340,
    height: 250,
    marginTop: 10,
    resizeMode: "stretch",
    marginLeft: 30
  },
  image: {
    // resizeMode: "cover",
    width: 384,
    height: 142,
    marginHorizontal: 20,
    marginRight: 20,
    marginTop: 10
  },
  imageCard: {
    // resizeMode: "cover",
    width: 384,
    height: 142,
    marginHorizontal: 20,
    backgroundColor: "#FFFFFF",
    borderWidth: 1,
    borderColor: "#F5F5F5",
    borderBottomEndRadius: 10,
    borderBottomLeftRadius: 10
  }
})
