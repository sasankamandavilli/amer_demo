// React Native Bottom Navigation
// https://aboutreact.com/react-native-bottom-navigation/

import * as React from "react"
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Pressable } from "react-native"
import Back from "./images/back2.svg"
import Angle from "./images/angle.svg"

const Library = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <ScrollView>
        <View style={styles.header}>
          <Pressable onPress={() => navigation.goBack()}>
            <Back height={22} width={12} />
          </Pressable>
          <Text style={styles.heading}>Library</Text>
          <View></View>
        </View>
        <View style={[styles.profileCard, { height: 284 }]}>
          <View style={[styles.cardInside, { marginTop: 10 }]}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Personal Photo</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>4</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Birth Certificate</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>3</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Education</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>8</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Marriage Certificate</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>1</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
        </View>
        <View style={[styles.profileCard, { height: 223 }]}>
          <View style={[styles.cardInside, { marginTop: 10 }]}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Passport</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>4</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Visa</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>3</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Emirates ID</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>8</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
        </View>
        <View style={[styles.profileCard, { height: 77 }]}>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Driving Licence</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>8</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
        </View>
        <View style={[styles.profileCard, { height: 77 }]}>
          <View style={styles.cardInside}>
            <View>
              <Text style={[styles.font, { fontSize: 17 }]}>Others</Text>
            </View>
            <Pressable onPress={() => navigation.navigate("Upload")}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 18, paddingHorizontal: 8 }]}>8</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </Pressable>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default Library
const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
    marginVertical: 10
  },
  heading: {
    fontSize: 18,
    fontFamily: "Roboto-Medium",
    fontWeight: "800"
  },
  profileCard: {
    width: 374,
    marginVertical: 10,
    backgroundColor: "#FFFFFF",
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#F5F5F5"
  },
  cardInside: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingVertical: 20
  }
})
