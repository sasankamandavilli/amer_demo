import "react-native-gesture-handler"
import * as React from "react"
import { useState } from "react"
import { Avatar } from "react-native-elements"
import { View, Text, StyleSheet, Modal, Pressable, TextInput } from "react-native"
import * as LAYOUT from "./styles"
import { SafeAreaView } from "react-native-safe-area-context"
import { ScrollView } from "react-native-gesture-handler"
import Upload from "./images/upload.svg"
import Down from "./images/back.svg"
import Back from "./images/back2.svg"
import Others from "./images/other.svg"
import Pay from "./images/pay.svg"
import Calender from "./images/calendar.svg"
import Front from "./images/front.svg"
import Close from "./images/close.svg"
import Folder from "./images/folder.svg"
import ServiceCenter from "./ServiceCenter"
import SelectService from "./SelectService"
import BookDate from "./BookDate"
import { RadioButton } from "react-native-paper"
import Apple from "./images/apple.svg"
import Visa from "./images/visa.svg"
import PayDark from "./images/pay2.svg"
import Plus from "./images/plus.svg"

const imageBack = require("D:/yash/reactnative/project/reactNativeAppSample/images/aptbgt.png")

function Home({ navigation }) {
  const [uploadModal, setUploadModal] = useState(false)
  const [centerModal, setCenterModal] = useState(false)
  const [serviceModal, setServiceModal] = useState(false)
  const [bookingModal, setBookingModal] = useState(false)
  const [paymentModal, setPaymentModal] = useState(false)
  const [cardModal, setCardModal] = useState(false)
  const [checked, setChecked] = React.useState("first")

  const sendChildToParent = dataFromChild => {
    setCenterModal(dataFromChild)
  }
  const sendServiceToParent = dataFromChild => {
    setServiceModal(dataFromChild)
  }
  const sendBookToParent = dataFromChild => {
    setBookingModal(dataFromChild)
  }

  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: "#000000CC" }}>
        <View style={styles.root}>
          <View style={[styles.heading, { marginBottom: 35, marginTop: 20 }]}>
            <Pressable onPress={() => navigation.goBack()}>
              <View>
                <Back height={20} width={12} />
              </View>
            </Pressable>
            <View>
              <Text style={{ fontFamily: "Segoe-UI", fontSize: 20, fontWeight: "900" }}>Service Booking</Text>
            </View>
            <View></View>
          </View>
          <View style={{ paddingHorizontal: 20 }}>
            <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>Service Name</Text>
          </View>
          <Pressable onPress={() => setServiceModal(true)}>
            <View style={[styles.heading, { marginBottom: 20 }]}>
              <View>
                <Text style={{ fontFamily: "Segoe-UI", fontSize: 20, fontWeight: "900" }}>Renew Emirates ID Card</Text>
              </View>

              <View>
                <Down height={6} width={12} />
              </View>
            </View>
          </Pressable>
          <View style={{ paddingHorizontal: 20 }}>
            <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>Service Center</Text>
          </View>
          <Pressable
            onPress={() => {
              setCenterModal(true)
              console.log(centerModal)
            }}
          >
            <View style={[styles.heading, { marginBottom: 30 }]}>
              <View>
                <Text style={{ fontFamily: "Segoe-UI", fontSize: 20, fontWeight: "900" }}>First Track Government System LLC</Text>
                <Text style={{ fontFamily: "Segoe-UI", fontSize: 14, fontWeight: "normal" }}>Al Waha Street,Al Quoz 1,Behind Oasis..</Text>
              </View>

              <View>
                <Down height={10} width={12} />
              </View>
            </View>
          </Pressable>
          <Pressable onPress={() => setBookingModal(true)}>
            <View style={styles.cardContainer}>
              <View style={styles.card}>
                <View>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>Date</Text>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 20, fontWeight: "bold" }}>Monday 11</Text>
                </View>
                <View>
                  <View>
                    <Calender height={22} width={22.5} />
                  </View>
                </View>
                <View>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>Time</Text>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 20, fontWeight: "bold" }}>8:00AM</Text>
                </View>
              </View>
            </View>
          </Pressable>
          <Pressable onPress={() => setUploadModal(true)}>
            <View style={styles.bigCard}>
              <View style={styles.cardInside}>
                <View style={[styles.heading, { marginVertical: 25 }]}>
                  <View>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 18, fontWeight: "900" }}>Required Documents(5)</Text>
                  </View>
                  <View>
                    <Upload height={18} width={22} />
                  </View>
                </View>
              </View>
              <Text style={{ marginLeft: 25, color: "white", fontSize: 12 }}>2/5 Documents uploaded</Text>
            </View>
          </Pressable>
          <View style={styles.bigCard}>
            <View style={styles.cardInside}>
              <View style={[styles.heading, { marginVertical: 25 }]}>
                <View>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 18, fontWeight: "900" }}>Additional Details</Text>
                </View>
                <View>
                  <Others width={28} height={24} />
                </View>
              </View>
            </View>
            <Text style={{ marginLeft: 25, color: "white", fontSize: 12 }}>All fields are Mandatory</Text>
          </View>
          <View style={{ marginTop: 30, paddingHorizontal: 20, marginBottom: 10 }}>
            <Text style={{ fontFamily: "Segoe-UI", fontSize: 24, fontWeight: "bold", padding: 10 }}>Payment</Text>
            <Pressable onPress={() => setPaymentModal(!paymentModal)}>
              <View style={styles.cardPayment}>
                <View style={styles.paymentInside}>
                  <View style={{ margin: 10 }}>
                    <Pay height={16} width={26} />
                  </View>
                  <View style={{ margin: 10 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "normal" }}>Pay at Center</Text>
                  </View>
                </View>
                <View style={{ margin: 10 }}>
                  <Down height={7} width={12} />
                </View>
              </View>
            </Pressable>
          </View>
          <Pressable onPress={() => navigation.navigate("Success")}>
            <View style={[styles.paymentButton, { paddingHorizontal: 20, marginBottom: 20 }]}>
              <View>
                <Text style={{ fontFamily: "Segoe-UI", fontSize: 20, fontWeight: "900", color: "#FFFFFF" }}>200 AED</Text>
              </View>
              <View style={styles.paymentInside}>
                <View>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 20, fontWeight: "900", color: "#FFFFFF" }}>Book Appointment</Text>
                </View>
                <View style={{ paddingHorizontal: 8 }}>
                  <Front height={12} width={7} />
                </View>
              </View>
            </View>
          </Pressable>
        </View>
      </ScrollView>
      {/* Documents Upload Modal Code */}
      <Modal animationType="slide" transparent={true} visible={uploadModal}>
        <View style={styles.modalView}>
          <View style={[styles.modalViewCard, { marginTop: 100, paddingHorizontal: 20 }]}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View></View>
              <View>
                <Text style={{ fontFamily: "Seoge-UI", fontSize: 16, fontWeight: "bold" }}>Upload Documents</Text>
              </View>

              <View>
                <Pressable onPress={() => setUploadModal(!uploadModal)}>
                  <Close height={12.5} width={12.5} />
                </Pressable>
              </View>
            </View>
            <View style={{ paddingTop: 5, alignItems: "center" }}>
              <Text style={{ fontSize: 12 }}>
                pdf, jpg, png, xls, doc {"  "}
                <Text style={{ color: "#707070" }}> |</Text> {"  "}Max. file size of each file 2MB
              </Text>
            </View>
            <View style={{ marginTop: 30, marginBottom: 10 }}>
              <Text style={{ fontSize: 14, color: "#40C381" }}>Passport Copy</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View style={{ width: 178, height: 60, backgroundColor: "#F7F7F7", borderRadius: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", margin: 10, marginHorizontal: 5, marginRight: 30 }}>
                  <View>
                    <Text style={{ fontSize: 16 }}>Upload</Text>
                    <Text style={{ fontSize: 12, color: "#5C5C5C" }}>From Phone</Text>
                  </View>
                  <View>
                    <Upload height={18} width={26} />
                  </View>
                </View>
              </View>
              <View style={{ width: 178, height: 60, backgroundColor: "#F7F7F7", borderRadius: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", margin: 10, marginHorizontal: 5, marginRight: 30 }}>
                  <View>
                    <Text style={{ fontSize: 16 }}>Choose</Text>
                    <Text style={{ fontSize: 12, color: "#5C5C5C" }}>From Library</Text>
                  </View>
                  <View>
                    <Folder height={19} width={24} />
                  </View>
                </View>
              </View>
            </View>
            <View style={{ marginTop: 20, marginBottom: 10 }}>
              <Text style={{ fontSize: 14, color: "#40C381" }}>Emirates ID Copy</Text>
            </View>
            <View style={{ width: 374, height: 60, backgroundColor: "#F7F7F7", borderRadius: 20 }}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", margin: 10, marginHorizontal: 5, marginRight: 30 }}>
                <View style={{ marginLeft: 5 }}>
                  <Text style={{ fontSize: 16, color: "#4E4E4E" }}>Emirates_id.pdf</Text>
                  <Text style={{ fontSize: 12, color: "#4E4E4E" }}>File size - 1 MB</Text>
                </View>
                <View>
                  <Close fill={"#40C381"} height={13} width={13} />
                </View>
              </View>
            </View>
            <Text style={{ fontSize: 14, color: "#5C5C5C", marginVertical: 10 }}>Uploaded From Library</Text>
            <View style={{ marginTop: 10, marginBottom: 15 }}>
              <Text style={{ fontSize: 14, color: "#40C381" }}>Other document</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View style={{ width: 178, height: 60, backgroundColor: "#F7F7F7", borderRadius: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", margin: 10, marginHorizontal: 5, marginRight: 30 }}>
                  <View>
                    <Text style={{ fontSize: 16 }}>Upload</Text>
                    <Text style={{ fontSize: 12, color: "#5C5C5C" }}>From Phone</Text>
                  </View>
                  <View>
                    <Upload height={18} width={26} />
                  </View>
                </View>
              </View>
              <View style={{ width: 178, height: 60, backgroundColor: "#F7F7F7", borderRadius: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", margin: 10, marginHorizontal: 5, marginRight: 30 }}>
                  <View>
                    <Text style={{ fontSize: 16 }}>Choose</Text>
                    <Text style={{ fontSize: 12, color: "#5C5C5C" }}>From Library</Text>
                  </View>
                  <View>
                    <Folder height={19} width={24} />
                  </View>
                </View>
              </View>
            </View>
            <View style={{ width: 374, height: 61, borderRadius: 30, backgroundColor: "#40C381", alignItems: "center", justifyContent: "center", marginTop: 60, marginBottom: 80 }}>
              <Text style={{ fontSize: 20, fontWeight: "bold", color: "#FFFFFF" }}>Done</Text>
            </View>
          </View>
        </View>
      </Modal>
      {/* Document Modal Code End */}
      <Modal animationType="slide" transparent={true} visible={centerModal}>
        <View style={styles.modalView}>
          <View style={[styles.modalViewCard, { marginTop: 10, paddingHorizontal: 20 }]}>
            <ServiceCenter sendChildToParent={sendChildToParent} />
          </View>
        </View>
      </Modal>
      <Modal animationType="slide" transparent={true} visible={serviceModal}>
        <View style={styles.modalView}>
          <View style={[styles.modalViewCard, { marginTop: 10, paddingHorizontal: 20 }]}>
            <SelectService sendServiceToParent={sendServiceToParent} />
          </View>
        </View>
      </Modal>
      <Modal animationType="slide" transparent={true} visible={bookingModal}>
        <View style={styles.modalView}>
          <View style={[styles.modalViewCard, { marginTop: 100, paddingHorizontal: 20 }]}>
            <BookDate sendBookToParent={sendBookToParent} />
          </View>
        </View>
      </Modal>
      <Modal animationType="slide" transparent={true} visible={paymentModal}>
        <View style={[styles.modalView, { height: LAYOUT.SCREEN_HEIGHT }]}>
          <View style={[styles.modalViewCard, { marginTop: (2 * LAYOUT.SCREEN_HEIGHT) / 3 - 120, height: 120 + (1 * LAYOUT.SCREEN_HEIGHT) / 3, paddingHorizontal: 20 }]}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View></View>
              <View>
                <Text style={{ fontFamily: "Roboto-Medium", fontSize: 16, fontWeight: "600" }}>Payment Method</Text>
              </View>

              <View>
                <Pressable onPress={() => setPaymentModal(!paymentModal)}>
                  <Close height={12.5} width={12.5} />
                </Pressable>
              </View>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: 10 }}>
              <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
                <PayDark height={16} width={26} />
                <Text style={{ fontFamily: "Roboto-Regular", fontSize: 18, fontWeight: "normal", marginHorizontal: 10 }}>Pay at Center</Text>
              </View>
              <View>
                <RadioButton color="#40C381" value="first" status={checked === "first" ? "checked" : "unchecked"} onPress={() => setChecked("first")} />
              </View>
            </View>
            <View style={styles.line}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: 10 }}>
              <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
                <Apple height={16} width={26} />
                <Text style={{ fontFamily: "Roboto-Regular", fontSize: 18, fontWeight: "normal", marginHorizontal: 10 }}>Apple Pay</Text>
              </View>
              <View>
                <RadioButton color="#40C381" value="second" status={checked === "second" ? "checked" : "unchecked"} onPress={() => setChecked("second")} />
              </View>
            </View>
            <View style={styles.line}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: 10 }}>
              <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
                <Visa height={16} width={26}></Visa>
                <Text style={{ fontFamily: "Roboto-Regular", fontSize: 18, fontWeight: "normal", marginHorizontal: 10 }}>Credit Card (FAB *** 8023)</Text>
              </View>
              <View>
                <RadioButton color="#40C381" value="third" status={checked === "third" ? "checked" : "unchecked"} onPress={() => setChecked("third")} />
              </View>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: 30 }}>
              <Text style={{ fontFamily: "Roboto-Regular", fontSize: 18, fontWeight: "normal", marginHorizontal: 10, color: "#40C381" }}>Add New Card </Text>
              <Pressable
                onPress={() => {
                  setPaymentModal(!paymentModal)
                  setCardModal(true)
                }}
              >
                <Plus style={{ marginRight: 30 }} size={16} />
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
      <Modal animationType="slide" transparent={true} visible={cardModal}>
        <View style={[styles.modalView, { height: LAYOUT.SCREEN_HEIGHT }]}>
          <View style={[styles.modalViewCard, { marginTop: (2 * LAYOUT.SCREEN_HEIGHT) / 3 - 160, height: 160 + (1 * LAYOUT.SCREEN_HEIGHT) / 3, paddingHorizontal: 20 }]}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: 15 }}>
              <View></View>
              <View>
                <Text style={{ fontFamily: "Roboto-Medium", fontSize: 16, fontWeight: "600" }}>Add Card</Text>
              </View>

              <View>
                <Pressable onPress={() => setCardModal(false)}>
                  <Close height={12.5} width={12.5} />
                </Pressable>
              </View>
            </View>
            <View style={[styles.line, { marginHorizontal: 0 }]}></View>
            <View style={styles.cardInput}>
              <TextInput placeholder="Name on Card"></TextInput>
            </View>
            <View style={styles.cardInput}>
              <TextInput placeholder="Card Number"></TextInput>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
              <View style={[styles.cardInput, { width: 160 }]}>
                <TextInput placeholder="Expiry Date (mm/yy)"></TextInput>
              </View>
              <View style={[styles.cardInput, { width: 150 }]}>
                <TextInput placeholder="CVV"></TextInput>
              </View>
            </View>
            <View style={styles.cardButton}>
              <Text style={{ fontFamily: "Roboto-Medium", fontSize: 20, fontWeight: "600", color: "#FFFFFF" }}>Add Card</Text>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  )
}
export default Home
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#FFFFFF"
    // margin: 10
  },
  modalRoot: {
    flex: 1
  },
  cardButton: {
    width: 374,
    height: 61,
    marginVertical: 30,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#40C381",
    borderRadius: 30
  },
  heading: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20
  },
  cardContainer: {
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 20,
    marginBottom: 15
  },
  card: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    width: 374,
    height: 89,
    borderColor: "#C3F3DA",
    borderWidth: 1,
    backgroundColor: "#EBFFF5",
    borderRadius: 10
  },
  cardPayment: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: 374,
    height: 68,
    borderColor: "#C3F3DA",
    borderWidth: 1,
    backgroundColor: "#FFFFFF",
    borderRadius: 10
  },
  paymentInside: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingLeft: 5 },
  bigCard: {
    marginHorizontal: 20,
    marginTop: 10,
    width: 374,
    height: 100,
    borderColor: "#B1EFCF",
    marginBottom: 15,
    borderRadius: 10,
    backgroundColor: "#65D09A"
  },
  cardInside: {
    width: 374,
    height: 80,
    backgroundColor: "#FFFFFF",
    borderColor: "#B1EFCF",
    borderWidth: 1,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20
  },
  paymentButton: {
    width: 374,
    height: 61,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#40C381",
    borderRadius: 30,
    margin: 10,
    marginHorizontal: 20
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    flex: 1,
    width: LAYOUT.SCREEN_WIDTH,
    backgroundColor: "#000000CC"
  },
  modalViewCard: {
    backgroundColor: "#FFFFFF",
    paddingTop: 20,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF"
  },
  buttonClose: {
    backgroundColor: "#2196F3"
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  line: {
    borderColor: "#F0F0F0",
    borderTopWidth: 1
  },
  cardInput: {
    borderColor: "#F0F0F0",
    borderBottomWidth: 1,
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#A0A0A0",
    paddingVertical: 15
  }
})
