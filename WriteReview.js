import * as React from "react"
import { View, Text, SafeAreaView, ScrollView, Pressable, StyleSheet, TextInput } from "react-native"
import * as LAYOUT from "./styles"
import { Avatar } from "react-native-elements"
import Icon from "react-native-vector-icons/FontAwesome"
import Back from "./images/back2.svg"
import Star from "./images/star5.svg"
import One from "./images/reviewstar.svg"

const WriteReview = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View style={styles.header}>
        <Pressable onPress={() => navigation.goBack()}>
          <Back marginRight={10} />
        </Pressable>
        <Avatar size={45} source={require("./images/defaultlogo.png")} activeOpacity={0.7} />
        <View style={{ marginHorizontal: 10 }}>
          <Text style={{ fontSize: 16, fontFamily: "Roboto-Regular", color: "#000000" }}>First Track Government System LLC</Text>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start" }}>
            <Text style={{ color: "#666666", fontSize: 14, fontFamily: "Roboto-Regular", marginRight: 3 }}>Over all rating :</Text>
            <Star height={11} width={11} />
            <Text style={{ color: "#666666", fontSize: 14, fontFamily: "Roboto-Regular", marginHorizontal: 3 }}>3.5</Text>
          </View>
        </View>
      </View>
      <ScrollView>
        <View style={{ marginTop: 20, marginHorizontal: 20 }}>
          <Text style={{ fontSize: 18, fontFamily: "Roboto-Medium", color: "#000000" }}>Rate the Center</Text>
          <View style={{ margin: 15, flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
          </View>
        </View>
        <View style={{ marginTop: 20, marginHorizontal: 20 }}>
          <Text style={{ fontSize: 18, fontFamily: "Roboto-Medium", color: "#000000" }}>Staff</Text>
          <View style={{ margin: 15, flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
          </View>
        </View>
        <View style={{ marginTop: 20, marginHorizontal: 20 }}>
          <Text style={{ fontSize: 18, fontFamily: "Roboto-Medium", color: "#000000" }}>Time of Processing</Text>
          <View style={{ margin: 15, flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
            <One height={30} width={31} marginHorizontal={10} />
          </View>
          <Text style={{ fontSize: 18, fontFamily: "Roboto-Medium", color: "#000000", marginVertical: 10 }}>Review Title</Text>
          <TextInput placeholder="Review" style={[styles.input, { height: 47, paddingHorizontal: 10 }]}></TextInput>
          <TextInput placeholder="Share your Experience" style={[styles.input, { height: 150, paddingHorizontal: 10, marginTop: 30 }]}></TextInput>
          <Text style={{ fontSize: 18, fontFamily: "Roboto-Medium", color: "#000000", marginTop: 25 }}>Add Photos</Text>
          <View style={{ width: 90, height: 72, backgroundColor: "#E3E3E3", borderWidth: 1, borderColor: "#E2E2E2", marginVertical: 20 }}></View>
          <Pressable onPress={() => navigation.navigate("Review")}>
            <View style={styles.submit}>
              <Text style={{ fontSize: 20, fontFamily: "Roboto-Medium", color: "#FFFFFF" }}>Submit a Review</Text>
            </View>
          </Pressable>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default WriteReview

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingHorizontal: 20,
    backgroundColor: "#FFFFFF",
    width: LAYOUT.SCREEN_WIDTH,
    height: 107,
    borderColor: "#FFFFFF",
    shadowRadius: 10,
    shadowColor: "#00000029"
  },
  input: {
    width: 374,
    borderColor: "#666666",
    borderRadius: 10,
    borderWidth: 1
  },
  submit: {
    height: 61,
    width: 374,
    backgroundColor: "#02B275",
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 50
  }
})
