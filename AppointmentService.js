// React Native Bottom Navigation
// https://aboutreact.com/react-native-bottom-navigation/

import * as React from "react"
import { View, Text, SafeAreaView, StyleSheet, Pressable, ScrollView, TextInput } from "react-native"
import Back from "./images/back2.svg"
import * as LAYOUT from "./styles"
import { Avatar } from "react-native-elements"
import Icon from "react-native-vector-icons/FontAwesome"
import Heart from "./images/heart.svg"

const AppointmentService = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView>
        <View style={styles.root}>
          <View style={styles.header}>
            <Pressable onPress={() => navigation.goBack()}>
              <Back width={12} height={20} />
            </Pressable>
            <Text style={{ fontSize: 16, fontFamily: "Roboto-Bold", fontWeight: "bold", paddingHorizontal: 20 }}>First Track Government System LLC</Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center", marginVertical: 15, paddingHorizontal: 20 }}>
            <View style={styles.searchInputContainer}>
              <Icon name="search" size={17} color={"#A7A7A7"} />
              <TextInput placeholder="Search" paddingHorizontal={10} />
            </View>
          </View>
          <Pressable onPress={() => navigation.navigate("ServiceBooking")}>
            <View style={styles.serviceCard}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                  <View>
                    <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                  </View>
                  <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                    <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                  </View>
                </View>
                <View style={{ marginVertical: 35 }}>
                  <Heart width={15.78} height={15.17} fill={"#40C381"} />
                </View>
              </View>
            </View>
          </Pressable>
          <View style={styles.serviceCard}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
              <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                <View>
                  <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                </View>
                <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                  <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                  <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                </View>
              </View>
              <View style={{ marginVertical: 35 }}>
                <Heart width={15.78} height={15.17} fill={"#40C381"} />
              </View>
            </View>
          </View>
          <View style={styles.serviceCard}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
              <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                <View>
                  <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                </View>
                <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                  <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                  <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                </View>
              </View>
              <View style={{ marginVertical: 35 }}>
                <Heart width={15.78} height={15.17} fill={"#40C381"} />
              </View>
            </View>
          </View>
          <View style={styles.serviceCard}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
              <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                <View>
                  <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                </View>
                <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                  <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                  <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                </View>
              </View>
              <View style={{ marginVertical: 35 }}>
                <Heart width={15.78} height={15.17} fill={"#40C381"} />
              </View>
            </View>
          </View>
          <View style={styles.serviceCard}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
              <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                <View>
                  <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                </View>
                <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                  <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                  <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                </View>
              </View>
              <View style={{ marginVertical: 35 }}>
                <Heart width={15.78} height={15.17} fill={"#40C381"} />
              </View>
            </View>
          </View>
          <View style={styles.serviceCard}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
              <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                <View>
                  <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                </View>
                <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                  <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                  <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                  <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                </View>
              </View>
              <View style={{ marginVertical: 35 }}>
                <Heart width={15.78} height={15.17} fill={"#40C381"} />
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default AppointmentService
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5"
  },
  header: {
    marginHorizontal: 20,
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  searchInputContainer: {
    height: 36,
    backgroundColor: "#f6f6f6",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    borderRadius: 12,
    width: 326
  },
  serviceCard: {
    width: 374,
    height: 108,
    backgroundColor: "#FFFFFF",
    borderWidth: 1,
    borderColor: "#F5F5F5",
    marginHorizontal: 20,
    marginVertical: 5,
    borderRadius: 20
  }
})
