import "react-native-gesture-handler"
import * as React from "react"
import { View, Text, StyleSheet, SafeAreaView, ScrollView, Pressable } from "react-native"
import { Avatar } from "react-native-elements"
import Close from "./images/close.svg"
import Upload from "./images/upload.svg"
//import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet"
import { TouchableOpacity } from "react-native-gesture-handler"

function Home({ navigation }) {
  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.root}>
          <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingVertical: 15 }}>
            <Pressable onPress={() => navigation.goBack()}>
              <Close height={18} width={18} />
            </Pressable>
            <Text style={styles.font}>Visa</Text>

            <View></View>
          </View>

          <TouchableOpacity>
            <View style={[styles.uploadButton, { paddingHorizontal: 20 }]}>
              <View style={{ flexDirection: "row" }}>
                <Upload height={21} width={26} />
                <Text style={{ paddingHorizontal: 15, fontFamily: "Segoe UI", fontSize: 16 }}>Upload</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.box}>
            <View style={styles.CardAmer}>
              <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
                <Avatar size={80} source={require("./images/layer.png")} activeOpacity={0.7} />
              </View>
              <View style={{ flexDirection: "column", justifyContent: "flex-start", padding: 10 }}>
                <Text style={{ fontFamily: "Segoe UI", fontSize: 16, fontWeight: "800" }}>Description Passport Visa Page</Text>
                <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", color: "#B7B7B7" }}>Passport_Scanned.pdf</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start" }}>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", color: "#7C7C7C" }}>256KB</Text>
                  <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", paddingHorizontal: 10 }}></View>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal", paddingHorizontal: 10, color: "#7C7C7C" }}>17/03/2022</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.CardAmer}>
              <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
                <Avatar size={80} source={require("./images/layer.png")} activeOpacity={0.7} />
              </View>
              <View style={{ flexDirection: "column", justifyContent: "flex-start", padding: 10 }}>
                <Text style={{ fontFamily: "Segoe UI", fontSize: 16, fontWeight: "800" }}>Description Passport Visa Page</Text>
                <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", color: "#B7B7B7" }}>Passport_Scanned.pdf</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start" }}>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", color: "#7C7C7C" }}>256KB</Text>
                  <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", paddingHorizontal: 10 }}></View>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal", paddingHorizontal: 10, color: "#7C7C7C" }}>17/03/2022</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.CardAmer}>
              <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
                <Avatar size={80} source={require("./images/layer.png")} activeOpacity={0.7} />
              </View>
              <View style={{ flexDirection: "column", justifyContent: "flex-start", padding: 10 }}>
                <Text style={{ fontFamily: "Segoe UI", fontSize: 16, fontWeight: "800" }}>Description Passport Visa Page</Text>
                <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", color: "#B7B7B7" }}>Passport_Scanned.pdf</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start" }}>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", color: "#7C7C7C" }}>256KB</Text>
                  <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", paddingHorizontal: 10 }}></View>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal", paddingHorizontal: 10, color: "#7C7C7C" }}>17/03/2022</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.CardAmer}>
              <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
                <Avatar size={80} source={require("./images/layer.png")} activeOpacity={0.7} />
              </View>
              <View style={{ flexDirection: "column", justifyContent: "flex-start", padding: 10 }}>
                <Text style={{ fontFamily: "Segoe UI", fontSize: 16, fontWeight: "800" }}>Description Passport Visa Page</Text>
                <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", color: "#B7B7B7" }}>Passport_Scanned.pdf</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start" }}>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 14, fontWeight: "normal", color: "#7C7C7C" }}>256KB</Text>
                  <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070", paddingHorizontal: 10 }}></View>
                  <Text style={{ fontFamily: "Segoe UI", fontSize: 12, fontWeight: "normal", paddingHorizontal: 10, color: "#7C7C7C" }}>17/03/2022</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default Home

const styles = StyleSheet.create({
  root: {
    backgroundColor: "#F5F5F5",
    paddingHorizontal: 15
  },
  uploadButton: {
    height: 91,
    width: 374,
    backgroundColor: "#FFFFFF",
    borderColor: "#40C381",
    borderWidth: 1,
    borderRadius: 10,
    borderStyle: "dashed",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10
  },
  font: {
    fontFamily: "Segoe-UI",
    fontSize: 18,
    fontWeight: "900"
  },
  box: {
    shadowColor: "#0000000D",
    width: 374,
    height: 114,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: "#FFFFFF",
    marginVertical: 10,
    borderColor: "#0000000D",
    direction: "ltr"
  },
  CardAmer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    paddingHorizontal: 25,
    paddingVertical: 10
  }
})
