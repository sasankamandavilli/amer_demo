import * as React from "react"
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Pressable } from "react-native"
import * as LAYOUT from "./styles"
import Back from "./images/back2.svg"
import Add from "./images/add.svg"
import Home from "./images/home.svg"
import Edit from "./images/edit.svg"
import Work from "./images/work.svg"
import { Avatar } from "react-native-elements"

const Profile = ({ navigation }) => {
  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.root}>
          <View style={styles.header}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "flex-start" }}>
              <Pressable onPress={() => navigation.goBack()}>
                <Back height={22} width={12} />
              </Pressable>
              <Avatar size={132} source={require("./images/profilepic.png")} activeOpacity={0.7} />
              <Edit size={17} />
            </View>
            <View style={{ alignItems: "center", marginVertical: 20 }}>
              <Text style={{ paddingVertical: 10, fontFamily: "Roboto-Medium", fontSize: 22, fontWeight: "600" }}>User Name</Text>
              <Text style={{ color: "#40C381", fontFamily: "Roboto-Regular", fontSize: 14, fontWeight: "normal" }}>Marasi drive, Business Bay, Dubai…</Text>
            </View>
          </View>
          <View style={styles.profileCard}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
              <View>
                <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>First Name</Text>
                <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600" }}>First Name</Text>
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginVertical: 10 }}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
              <View>
                <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Last Name</Text>
                <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600" }}>Last Name</Text>
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginVertical: 10 }}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
              <View>
                <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Mobile Number</Text>
                <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600" }}>+971 123456789</Text>
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginVertical: 10 }}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
              <View>
                <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Email</Text>
                <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600" }}>email@email.com</Text>
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginVertical: 10 }}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
              <View>
                <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Gender</Text>
                <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600" }}>Male</Text>
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginVertical: 10 }}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
              <View>
                <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Marital Status</Text>
                <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600" }}>Single</Text>
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginVertical: 10 }}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 10, marginVertical: 5 }}>
              <View>
                <Text style={{ color: "#5A5A5A", fontSize: 12, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Date of Birth</Text>
                <Text style={{ color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600" }}>08 may 1993</Text>
              </View>
            </View>
          </View>
          <Text style={{ margin: 20, color: "#0B0B0B", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600" }}> Adress</Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.buttonGroup}>
              <View style={styles.addButton}>
                <Add size={35} />
                <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal", marginVertical: 10 }}>Add</Text>
              </View>
              <View style={styles.address}>
                <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
                  <Home />
                  <Text style={{ color: "#000000", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600", marginHorizontal: 10 }}>Home</Text>
                </View>
                <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal", marginVertical: 10 }}>24,1 floor, 56B Street, Al Barsha</Text>
                <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Dubai</Text>
              </View>
              <View style={styles.address}>
                <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
                  <Work />
                  <Text style={{ color: "#000000", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600", marginHorizontal: 10 }}>Work</Text>
                </View>
                <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal", marginVertical: 10 }}>24,1 floor, 56B Street, Al Barsha</Text>
                <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Dubai</Text>
              </View>
              <View style={styles.address}>
                <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
                  <Home />
                  <Text style={{ color: "#000000", fontSize: 18, fontFamily: "Roboto-Medium", fontWeight: "600", marginHorizontal: 10 }}>Others</Text>
                </View>
                <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal", marginVertical: 10 }}>24,1 floor, 56B Street, Al Barsha</Text>
                <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Dubai</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default Profile

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#F5F5F5"
  },
  header: {
    width: LAYOUT.SCREEN_WIDTH,
    height: 273,
    backgroundColor: "#FFFFFF",
    borderBottomEndRadius: 30,
    borderBottomStartRadius: 30,

    paddingHorizontal: 20,
    paddingTop: 30
  },
  profileCard: {
    width: 374,
    height: 510,
    backgroundColor: "#FFFFFF",
    marginTop: 15,
    marginHorizontal: 20,

    borderRadius: 20,
    padding: 10
  },
  addButton: {
    height: 118,
    width: 79,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    marginHorizontal: 5
  },
  address: {
    height: 118,
    width: 230,
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    marginHorizontal: 5,
    padding: 14
  },
  buttonGroup: {
    flexDirection: "row",
    marginHorizontal: 20,
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 45
  }
})
