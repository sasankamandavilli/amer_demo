import React from "react"
import { View, Text, StyleSheet, SafeAreaView, ScrollView, Pressable, Platform, Dimensions } from "react-native"
import * as LAYOUT from "./styles"
import { Avatar } from "react-native-elements"
import Angle from "./images/angle.svg"
import { useSelector, useDispatch } from "react-redux"
import { switchDarkMode } from "./store/actions/DarkModeAction"
import { switchDirection } from "./store/actions/DirectionAction"
import AsyncStorage from "@react-native-async-storage/async-storage"
//local import
import Strings from "./utilities/i18nStrings"

const Settings = ({ navigation }) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.card}>
            <View style={styles.cardInside}>
              <View style={{ padding: 10 }}>
                <Text style={styles.bigBlue}>UserName</Text>
                <View style={styles.location}>
                  <Text style={styles.normal}>username@user.com</Text>
                </View>
              </View>
              <View style={{ paddingRight: 30, paddingTop: 5 }}>
                <Avatar size={40} source={require("./images/profile.png")} activeOpacity={0.7} />
              </View>
            </View>
          </View>
          <Pressable onPress={() => navigation.navigate("SettingStack")}>
            <View style={styles.cardLib}>
              <View style={styles.cardInside}>
                <View>
                  <Text style={[styles.font, { color: "#40C381", fontSize: 20 }]}>My Library</Text>
                  <Text>8 Documents</Text>
                </View>
                <View>
                  <Avatar size={31} source={require("./images/library.png")} activeOpacity={0.7} />
                </View>
              </View>
            </View>
          </Pressable>
          <View style={styles.profileCard}>
            <View style={styles.cardInside}>
              <View>
                <Text style={[styles.font, { fontSize: 17 }]}>Enable Face ID</Text>
              </View>
              <View>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
            <View style={styles.cardInside}>
              <View>
                <Text style={[styles.font, { fontSize: 17 }]}>Language</Text>
              </View>
              <Pressable onPress={() => navigation.navigate("LanguagePicker")}>
                <View style={{ flexDirection: "row" }}>
                  <Text style={[styles.font, { color: "#C1C1C1", fontSize: 14, paddingHorizontal: 8 }]}>English</Text>
                  <Angle height={15} width={9} fill={"#666666"} />
                </View>
              </Pressable>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
            <View style={styles.cardInside}>
              <View>
                <Text style={[styles.font, { fontSize: 17 }]}>Change Password</Text>
              </View>
              <View>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </View>
          </View>
          <View style={styles.settingCard}>
            <View style={styles.cardInside}>
              <View>
                <Text style={[styles.font, { fontSize: 17 }]}>About</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text style={[styles.font, { color: "#C1C1C1", fontSize: 14, paddingHorizontal: 8 }]}>Version 3.0</Text>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
            <View style={styles.cardInside}>
              <View>
                <Text style={[styles.font, { fontSize: 17 }]}>Tutorials</Text>
              </View>
              <View>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
            <View style={styles.cardInside}>
              <View>
                <Text style={{ fontFamily: "SegoeUIBold", fontSize: 17 }}>Rate App</Text>
              </View>
              <View>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
            <View style={styles.cardInside}>
              <View>
                <Text style={[styles.font, { fontSize: 17 }]}>Contact</Text>
              </View>
              <View>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </View>
            <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginHorizontal: 20 }}></View>
            <View style={styles.cardInside}>
              <View>
                <Text style={[styles.font, { fontSize: 17 }]}>Feedback</Text>
              </View>
              <View>
                <Angle height={15} width={9} fill={"#666666"} />
              </View>
            </View>
          </View>
          <View style={styles.signOut}>
            <Text style={{ color: "#FD0D1B", fontSize: 17 }}>Sign Out</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
class StyleSheetFactory {
  static getSheet(direction) {
    return StyleSheet.create({
      rowContainer: {
        backgroundColor: "#F5F5F5",
        marginLeft: 0,
        paddingLeft: 10,

        paddingRight: 10,
        display: "flex",
        height: 65,
        justifyContent: "space-between",
        flexDirection: Platform.OS === "android" ? (direction === "rtl" ? "row-reverse" : "row") : "row",
        direction: direction,
        alignItems: "center"
      },
      row: {
        flexDirection: Platform.OS === "android" ? (direction === "rtl" ? "row-reverse" : "row") : "row",
        direction: direction,
        alignItems: "center"
      },
      IconBg: {
        width: 40,
        height: 40,
        borderRadius: 20,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.profileIconBg
      },
      LabelText: {
        fontSize: 15,
        color: theme.primaryLabel,
        paddingLeft: 10,
        fontFamily: Strings.fontRegular,
        paddingRight: 10
      }
    })
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#F5F5F5",
    width: LAYOUT.SCREEN_WIDTH
  },
  signOut: {
    width: 374,
    height: 63,
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    borderWidth: 1,
    borderColor: "#F5F5F5",
    backgroundColor: "#FFFFFF",
    marginHorizontal: 20,
    borderRadius: 20
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8
  },
  header: {
    fontSize: 32,
    backgroundColor: "#fff"
  },
  title: {
    fontSize: 24
  },
  bigBlue: {
    color: "black",
    fontWeight: "bold",
    fontSize: 22
  },
  normal: {
    color: "black",
    fontSize: 12
  },
  card: {
    width: LAYOUT.SCREEN_WIDTH,
    height: LAYOUT.SCREEN_HEIGHT / 8,
    backgroundColor: "#FFFFFF",
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderColor: "red",
    flexDirection: "column"
  },
  cardInside: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  left: {
    flexDirection: "row-reverse"
  },
  location: {
    flexDirection: "row"
  },
  searchInputContainer: {
    height: 50,
    backgroundColor: "#f6f6f6",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    borderRadius: 12
  },
  stretch: {
    width: 500,
    height: 200,
    resizeMode: "stretch"
  },
  tinyLogo: {
    width: 370,
    height: 300,
    marginTop: 10,
    resizeMode: "stretch",
    marginLeft: 20
  },
  cardLib: {
    width: 374,
    marginVertical: 10,
    height: 83,
    backgroundColor: "#FFFFFF",
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#F5F5F5"
  },
  cardInside: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingVertical: 15
  },
  profileCard: {
    width: 374,
    marginVertical: 10,
    height: 190,
    backgroundColor: "#FFFFFF",
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#F5F5F5"
  },
  settingCard: {
    width: 374,
    marginVertical: 10,
    height: 300,
    backgroundColor: "#FFFFFF",
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#F5F5F5"
  },
  font: {
    fontFamily: "Segoe-UI Bold",
    fontWeight: "800"
  }
})

export default Settings
