import "react-native-gesture-handler"
import * as React from "react"
import { Avatar } from "react-native-elements"
import { View, Text, StyleSheet, TextInput, Pressable } from "react-native"
import { SafeAreaView } from "react-native-safe-area-context"
import { ScrollView } from "react-native-gesture-handler"
import Icon from "react-native-vector-icons/FontAwesome"
import Back from "./images/back.svg"
import { useState } from "react"
import Cancel from "./images/close.svg"
import * as LAYOUT from "./styles"

function Home({ sendChildToParent }) {
  const [center, setCenter] = useState(false)

  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.root}>
          <View style={styles.heading}>
            <View style={styles.location}>
              <View>
                <Icon.Button name="heart" color={"#40C381"} backgroundColor="transparent" marginLeft={-8} />
              </View>
              <View>
                <Text>Mirdif</Text>
              </View>
              <View style={{ padding: 5 }}>
                <Back height={7} width={12} />
              </View>
            </View>
            <Pressable
              onPress={() => {
                setCenter(true)
                console.log(center)
                sendChildToParent(center)
              }}
            >
              <View>
                <Cancel size={12.5} />
              </View>
            </Pressable>
          </View>
          <View style={[styles.line, { marginHorizontal: 0 }]}></View>
          <View style={styles.searchInputContainer}>
            <Icon name="search" color={"#A7A7A7"} size={25} />
            <TextInput placeholder="Search" />
          </View>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
            <View style={styles.buttonGroup}>
              <View style={styles.buttonStart}>
                <Text style={{ fontFamily: "Helvetica Neue", fontSize: 14, fontWeight: "normal", color: "#149639" }}>Near to Me</Text>
              </View>
              <View style={styles.button}>
                <Text style={{ fontFamily: "Helvetica Neue", fontSize: 14, fontWeight: "normal", color: "#707070" }}>Top Rated</Text>
              </View>
              <View style={styles.button}>
                <Text style={{ fontFamily: "Helvetica Neue", fontSize: 14, fontWeight: "normal", color: "#707070" }}>Waiting Time</Text>
              </View>
              <View style={styles.button}>
                <Text style={{ fontFamily: "Helvetica Neue", fontSize: 14, fontWeight: "normal", color: "#707070" }}>Others</Text>
              </View>
              <View style={styles.button}>
                <Text style={{ fontFamily: "Helvetica Neue", fontSize: 14, fontWeight: "normal", color: "#707070" }}>Others</Text>
              </View>
            </View>
          </ScrollView>
          <View style={styles.CardAmer}>
            <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
              <Avatar size={80} source={require("./images/amer.png")} activeOpacity={0.7} />
            </View>
            <View style={{ flexDirection: "column", justifyContent: "flex-start" }}>
              <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "800" }}>First Track Govenment {"\n"} Transaction</Text>
              <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", marginTop: 3 }}>Al What Street,Al Quoz 1,Behind Oasis</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 3 }}>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>3 Km Away</Text>
                <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070" }}></View>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal" }}>+971-123456789</Text>
              </View>
            </View>
            <View style={styles.starButton}>
              <Avatar size={10} source={require("./images/star.png")} activeOpacity={0.7} />
              <Text style={{ fontSize: 12, fontFamily: "Roboto", color: "#FFFFFF", padding: 2 }}>4.5</Text>
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginLeft: 120, marginRight: 20 }}></View>
          <View style={styles.CardAmer}>
            <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
              <Avatar size={80} source={require("./images/amer.png")} activeOpacity={0.7} />
            </View>
            <View style={{ flexDirection: "column", justifyContent: "flex-start" }}>
              <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "800" }}>First Track Govenment {"\n"} Transaction</Text>
              <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", marginTop: 3 }}>Al What Street,Al Quoz 1,Behind Oasis</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 3 }}>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>3 Km Away</Text>
                <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070" }}></View>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal" }}>+971-123456789</Text>
              </View>
            </View>
            <View style={styles.starButton}>
              <Avatar size={10} source={require("./images/star.png")} activeOpacity={0.7} />
              <Text style={{ fontSize: 12, fontFamily: "Roboto", color: "#FFFFFF", padding: 2 }}>4.5</Text>
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginLeft: 120, marginRight: 20 }}></View>
          <View style={styles.CardAmer}>
            <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
              <Avatar size={80} source={require("./images/amer.png")} activeOpacity={0.7} />
            </View>
            <View style={{ flexDirection: "column", justifyContent: "flex-start" }}>
              <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "800" }}>First Track Govenment {"\n"} Transaction</Text>
              <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", marginTop: 3 }}>Al What Street,Al Quoz 1,Behind Oasis</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 3 }}>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>3 Km Away</Text>
                <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070" }}></View>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal" }}>+971-123456789</Text>
              </View>
            </View>
            <View style={styles.starButton}>
              <Avatar size={10} source={require("./images/star.png")} activeOpacity={0.7} />
              <Text style={{ fontSize: 12, fontFamily: "Roboto", color: "#FFFFFF", padding: 2 }}>4.5</Text>
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginLeft: 120, marginRight: 20 }}></View>
          <View style={styles.CardAmer}>
            <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
              <Avatar size={80} source={require("./images/amer.png")} activeOpacity={0.7} />
            </View>
            <View style={{ flexDirection: "column", justifyContent: "flex-start" }}>
              <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "800" }}>First Track Govenment {"\n"} Transaction</Text>
              <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", marginTop: 3 }}>Al What Street,Al Quoz 1,Behind Oasis</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 3 }}>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>3 Km Away</Text>
                <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070" }}></View>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal" }}>+971-123456789</Text>
              </View>
            </View>
            <View style={styles.starButton}>
              <Avatar size={10} source={require("./images/star.png")} activeOpacity={0.7} />
              <Text style={{ fontSize: 12, fontFamily: "Roboto", color: "#FFFFFF", padding: 2 }}>4.5</Text>
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginLeft: 120, marginRight: 20 }}></View>
          <View style={styles.CardAmer}>
            <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
              <Avatar size={80} source={require("./images/amer.png")} activeOpacity={0.7} />
            </View>
            <View style={{ flexDirection: "column", justifyContent: "flex-start" }}>
              <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "800" }}>First Track Govenment {"\n"} Transaction</Text>
              <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", marginTop: 3 }}>Al What Street,Al Quoz 1,Behind Oasis</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 3 }}>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>3 Km Away</Text>
                <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070" }}></View>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal" }}>+971-123456789</Text>
              </View>
            </View>
            <View style={styles.starButton}>
              <Avatar size={10} source={require("./images/star.png")} activeOpacity={0.7} />
              <Text style={{ fontSize: 12, fontFamily: "Roboto", color: "#FFFFFF", padding: 2 }}>4.5</Text>
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginLeft: 120, marginRight: 20 }}></View>
          <View style={styles.CardAmer}>
            <View style={{ borderWidth: 1, borderColor: "#F0F0F0", borderRadius: 5 }}>
              <Avatar size={80} source={require("./images/amer.png")} activeOpacity={0.7} />
            </View>
            <View style={{ flexDirection: "column", justifyContent: "flex-start" }}>
              <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "800" }}>First Track Govenment {"\n"} Transaction</Text>
              <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", marginTop: 3 }}>Al What Street,Al Quoz 1,Behind Oasis</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 3 }}>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal", color: "#159A47" }}>3 Km Away</Text>
                <View style={{ height: 12, borderRightWidth: 1, borderColor: "#707070" }}></View>
                <Text style={{ fontFamily: "Roboto", fontSize: 12, fontWeight: "normal" }}>+971-123456789</Text>
              </View>
            </View>
            <View style={styles.starButton}>
              <Avatar size={10} source={require("./images/star.png")} activeOpacity={0.7} />
              <Text style={{ fontSize: 12, fontFamily: "Roboto", color: "#FFFFFF", padding: 2 }}>4.5</Text>
            </View>
          </View>
          <View style={{ borderTopWidth: 2, borderColor: "#F0F0F0", height: 1, marginLeft: 120, marginRight: 20 }}></View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default Home
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  starButton: { flexDirection: "row", justifyContent: "center", alignItems: "center", backgroundColor: "#40C381", height: 23, width: 47, borderRadius: 10 },
  CardAmer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",

    paddingVertical: 10
  },
  serviceCard: {
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  buttonStart: {
    width: 99,
    height: 35,
    borderRadius: 15,
    borderColor: "#40C381",
    borderWidth: 1,
    margin: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    width: 99,
    height: 35,
    borderRadius: 15,
    margin: 10,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F5F5F5"
  },
  heading: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",

    paddingVertical: 10
  },
  cardContainer: {
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 20
  },
  card: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    width: 374,
    height: 89,
    borderColor: "#C3F3DA",
    borderWidth: 1,
    backgroundColor: "#EBFFF5",
    borderRadius: 10
  },
  cardPayment: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: 374,
    height: 68,
    borderColor: "#C3F3DA",
    borderWidth: 1,
    backgroundColor: "#FFFFFF",
    borderRadius: 10
  },
  paymentInside: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingLeft: 5 },
  doubleCard: {
    marginTop: 10,
    flexDirection: "column",
    alignItems: "center"
  },
  cardTop: {
    width: 374,
    height: 79,
    borderColor: "#B1EFCF",
    borderWidth: 1,
    borderRadius: 10
  },
  cardBottom: {
    width: 374,
    height: 21,
    borderWidth: 1,
    borderColor: "#B1EFCF",
    backgroundColor: "#65D09A",
    borderBottomEndRadius: 10,
    borderBottomLeftRadius: 10
  },
  paymentButton: {
    width: 374,
    height: 61,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#40C381",
    borderRadius: 30,
    margin: 10,
    paddingHorizontal: 10
  },
  location: {
    flexDirection: "row",
    alignItems: "center"
  },
  line: {
    borderColor: "#F0F0F0",
    borderTopWidth: 1,
    width: LAYOUT.SCREEN_WIDTH
  },
  searchInputContainer: {
    height: 36,
    backgroundColor: "#f6f6f6",
    flexDirection: "row",
    alignItems: "center",

    borderRadius: 12,

    marginVertical: 15
  },
  buttonGroup: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
})
