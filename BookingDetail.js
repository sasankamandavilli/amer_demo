import "react-native-gesture-handler"
import * as React from "react"
import { View, Text, StyleSheet, SafeAreaView, ScrollView, Pressable } from "react-native"
import { Avatar } from "react-native-elements"
import * as LAYOUT from "./styles"
import Close from "./images/close.svg"
import Up from "./images/up.svg"
import Trash from "./images/trash.svg"
import Distance from "./images/distance.svg"

function Home({ navigation }) {
  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.root}>
          <Pressable onPress={() => navigation.goBack()}>
            <View style={{ marginHorizontal: 20, marginTop: 20, marginBottom: 10 }}>
              <Close height={18} width={18} fill={"#FFFFFF"} />
            </View>
          </Pressable>
          <View style={{ justifyContent: "center", alignItems: "center", marginBottom: 10 }}>
            <Text style={{ fontSize: 38, fontFamily: "Segoe-UI", fontWeight: "bold", color: "#FFFFFF" }}>Q 19001</Text>
          </View>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontSize: 26, fontFamily: "Segoe-UI", fontWeight: "900", color: "#FFFFFF" }}>08:00 Aam </Text>
          </View>
          <View style={{ justifyContent: "center", alignItems: "center", marginBottom: 30 }}>
            <Text style={{ fontSize: 26, fontFamily: "Segoe-UI", fontWeight: "900", color: "#FFFFFF" }}>Monday, 11 May 2020</Text>
          </View>
          <View style={styles.card}>
            <View style={{ width: 88, height: 23, backgroundColor: "#D9FFE7", borderRadius: 10, justifyContent: "center", alignItems: "center", marginTop: 30, marginHorizontal: 20 }}>
              <Text style={{ color: "#007E2C" }}>Approved</Text>
            </View>
            <View style={{ marginHorizontal: 20, marginTop: 15 }}>
              <Text style={{ fontSize: 24, fontFamily: "Segoe-UI", fontWeight: "bold" }}>First Track Government Transaction</Text>
            </View>
            <View style={{ marginHorizontal: 20, marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View>
                <Text style={{ fontSize: 14, fontFamily: "Segoe-UI" }}>Al What Street, Al Quoz 1, Behind Oasis</Text>
                <Text style={{ fontSize: 14, fontFamily: "Segoe-UI" }}>+971 123456789</Text>
              </View>
              <View style={{ width: 77, height: 26, borderRadius: 20, backgroundColor: "#40C381", marginRight: 10, borderColor: "#40C381" }}>
                <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginTop: 5 }}>
                  <Distance height={14.75} width={14.75}></Distance>
                  <Text style={{ color: "#000000", fontSize: 13, marginHorizontal: 3 }}>2 KM</Text>
                </View>
              </View>
            </View>
            <View style={{ marginHorizontal: 20, marginTop: 25, marginBottom: 15 }}>
              <Text style={{ fontSize: 18, fontFamily: "Segoe-UI", fontWeight: "bold" }}>Documents Uploaded (5/5)</Text>
            </View>

            <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 5 }}>
              <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                <View style={styles.documentCard}>
                  <Avatar size={124} source={require("./images/layer.png")} activeOpacity={0.7} />
                </View>
                <View style={styles.documentCard}>
                  <Avatar size={124} source={require("./images/layer.png")} activeOpacity={0.7} />
                </View>
                <View style={styles.documentCard}>
                  <Avatar size={124} source={require("./images/layer.png")} activeOpacity={0.7} />
                </View>
                <View style={styles.documentCard}>
                  <Avatar size={124} source={require("./images/layer.png")} activeOpacity={0.7} />
                </View>
                <View style={styles.documentCard}>
                  <Avatar size={124} source={require("./images/layer.png")} activeOpacity={0.7} />
                </View>
              </ScrollView>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 20, marginTop: 20, alignItems: "center", marginBottom: 25 }}>
              <Text style={{ fontSize: 18, fontFamily: "Segoe-UI", fontWeight: "bold" }}>Additional Details</Text>
              <Up />
            </View>
            <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
              <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "normal", color: "#A0A0A0" }}>Name</Text>
              <Text style={{ fontSize: 16, fontFamily: "Segoe-UI", fontWeight: "900" }}>Applicant Name</Text>
            </View>
            <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
              <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "normal", color: "#A0A0A0" }}>Relation</Text>
              <Text style={{ fontSize: 16, fontFamily: "Segoe-UI", fontWeight: "900" }}>Relations</Text>
            </View>
            <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
              <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "normal", color: "#A0A0A0" }}>Passport Expiry Date</Text>
              <Text style={{ fontSize: 16, fontFamily: "Segoe-UI", fontWeight: "900" }}>10/12/2025</Text>
            </View>
            <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
              <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "normal", color: "#A0A0A0" }}>Other</Text>
              <Text style={{ fontSize: 16, fontFamily: "Segoe-UI", fontWeight: "900" }}>Other</Text>
            </View>
            <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
              <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "normal", color: "#A0A0A0" }}>Other</Text>
              <Text style={{ fontSize: 16, fontFamily: "Segoe-UI", fontWeight: "600", color: "#000000" }}>Other</Text>
            </View>
            <View style={{ width: LAYOUT.SCREEN_WIDTH, height: 80, borderColor: "#D3D3D3", borderWidth: 1, justifyContent: "center", alignItems: "baseline" }}>
              <View style={{ flexDirection: "row", marginHorizontal: 20, alignItems: "center" }}>
                <Trash width={16} height={18} />
                <Text style={{ color: "#E62235", fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18, paddingHorizontal: 10 }}>Cancel Booking</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default Home

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#40C381"
  },
  card: {
    backgroundColor: "#FFFFFF",
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20
  },
  documentCard: {
    height: 149,
    width: 138,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#D3D3D3",
    borderRadius: 20,
    marginHorizontal: 10
  }
})
