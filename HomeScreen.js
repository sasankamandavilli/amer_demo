import React from "react"
import { useState } from "react"
import { SafeAreaView, View, StatusBar, Text, TextInput, FlatList, Dimensions, StyleSheet, Image, Pressable, ScrollView, TouchableOpacity, ImageBackground, TouchableHighlight, Button, Alert } from "react-native"
//import COLORS from "../../consts/colors"
import Icon from "react-native-vector-icons/MaterialIcons"
const { width } = Dimensions.get("screen")
import { Avatar, FAB } from "react-native-elements"
import * as LAYOUT from "./styles"
import Notification from "./images/notification.svg"
import MostUsed from "./images/mostused.svg"
import Near from "./images/centersnear.svg"
import AppointmentLogo from "./images/bookappointment.svg"

//import houses from "../../consts/houses"
const COLORS = {
  white: "#FFF",
  dark: "#000",
  light: "#f6f6f6",
  grey: "#A9A9A9",
  blue: "#5f82e6",
  red: "red",
  tranparent: "rgba(0,0,0,0)"
}
const imageTwo = require("D:/yash/reactnative/project/reactNativeAppSample/images/img2.png")
const imageOne = require("D:/yash/reactnative/project/reactNativeAppSample/images/img.png")
const imageThird = require("D:/yash/reactnative/project/reactNativeAppSample/images/img3.png")
const imageBack = require("D:/yash/reactnative/project/reactNativeAppSample/images/aptbgt.png")

const HomeScreen = ({ navigation }) => {
  const [count, setCount] = useState(0)

  return (
    <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
      {/* Customise status bar */}
      <StatusBar translucent={false} backgroundColor={COLORS.white} barStyle="dark-content" />
      {/* Header container */}

      <ScrollView showsVerticalScrollIndicator={false} stickyHeaderIndices={[5]}>
        {/* <View style={{ flexDirection: "row" }}>
          <Button title="Increment" />
          <Text>{count}</Text>
          <Button title="Decrement" onPress={() => Alert.alert("Decrement Button pressed")} />
        </View> */}
        <View style={style.card}>
          <View style={style.header}>
            <Pressable onPress={() => navigation.navigate("Profile")}>
              <View style={{ flexDirection: "row" }}>
                <View>
                  <Avatar size={40} source={require("./images/profile.png")} activeOpacity={0.7} />
                </View>
                <View style={{ paddingHorizontal: 5, height: 40 }}>
                  <Text style={{ color: "#000000", fontSize: 14, fontFamily: "Roboto-Regular" }}>Welcome</Text>
                  <Text style={{ color: "#000000", fontSize: 18, fontFamily: "Roboto-Bold", fontWeight: "bold" }}>User Name</Text>
                </View>
              </View>
            </Pressable>
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
              <View style={{ paddingHorizontal: 27 }}>
                <Notification width={32} height={32}></Notification>
              </View>
              <View>
                <Avatar size={32} source={require("./images/face.png")} activeOpacity={0.7} />
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              paddingHorizontal: 20
            }}
          >
            <View style={style.searchInputContainer}>
              <Icon name="search" color={COLORS.grey} size={25} />
              <TextInput placeholder="Search" />
            </View>
          </View>
        </View>
        {/* Input and sort button container */}
        <View style={{ backgroundColor: "#EFEFF4", height: 320 }}>
          <View>
            <Image style={style.tinyLogo} source={require("./images/video.png")} />
          </View>
          <TouchableOpacity
            style={style.loginBtn}
            onPress={() => {
              navigation.navigate("Services")
            }}
          >
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
              <AppointmentLogo width={24} height={25} />
              <Text style={{ color: "#FFFFFF", fontSize: 22, fontFamily: "Segoe-UI", fontWeight: "600", paddingHorizontal: 10 }}>Book an Appointment</Text>
            </View>
          </TouchableOpacity>
        </View>

        {/* Render list options */}
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={style.optionListsContainer}>
            <Pressable onPress={() => navigation.navigate("Services")}>
              <View style={style.optionsCard}>
                <ImageBackground source={imageOne} style={style.image}>
                  <ImageBackground source={imageBack} style={[style.imageBack]} imageStyle={{ borderRadius: 20 }}>
                    <View style={{ flexDirection: "column", justifyContent: "flex-start", padding: 20 }}>
                      <View style={{ width: 43, height: 43, borderRadius: 43, backgroundColor: "#FD5D22", alignItems: "center", justifyContent: "center" }}>
                        <MostUsed width={17} height={22} />
                      </View>

                      <Text style={{ marginTop: 10, fontSize: 30, fontWeight: "bold", color: "#FFFFFF" }}>Most Used Services</Text>
                    </View>
                  </ImageBackground>
                </ImageBackground>
              </View>
            </Pressable>
            <Pressable onPress={() => navigation.navigate("Centers")}>
              <View style={style.optionsCard}>
                <ImageBackground source={imageTwo} style={style.image}>
                  <ImageBackground source={imageBack} style={[style.imageBack]} imageStyle={{ borderRadius: 20 }}>
                    <View style={{ flexDirection: "column", justifyContent: "flex-start", padding: 20 }}>
                      <View style={{ width: 43, height: 43, borderRadius: 43, backgroundColor: "#44D075", alignItems: "center", justifyContent: "center" }}>
                        <Near width={17} height={22} />
                      </View>

                      <Text style={{ marginTop: 10, fontSize: 30, fontWeight: "bold", color: "#FFFFFF" }}>Centers Near{"\n"}to You</Text>
                    </View>
                  </ImageBackground>
                </ImageBackground>
              </View>
            </Pressable>
            <View style={style.optionsCard}>
              <ImageBackground source={imageThird} style={style.image}>
                <ImageBackground source={imageBack} style={[style.imageBack]} imageStyle={{ borderRadius: 20 }}>
                  <View style={{ flexDirection: "column", justifyContent: "flex-start", padding: 20 }}>
                    <View style={{ width: 43, height: 43, borderRadius: 43, backgroundColor: "#FFE200", alignItems: "center", justifyContent: "center" }}>
                      <MostUsed width={17} height={22} />
                    </View>

                    <Text style={{ marginTop: 10, fontSize: 30, fontWeight: "bold", color: "#FFFFFF" }}>Others</Text>
                  </View>
                </ImageBackground>
              </ImageBackground>
            </View>
          </View>
        </ScrollView>
        <Pressable onPress={() => navigation.navigate("Bookings")}>
          <View style={{ backgroundColor: "#EFEFF4" }}>
            <ImageBackground style={style.tinyLogo} source={require("./images/bookcard.png")}>
              <View style={{ marginLeft: 110, marginTop: 25 }}>
                <Text style={{ color: "#40C381", fontSize: 18, fontFamily: "Segoe-UI" }}>My Bookings</Text>
                <Text style={{ fontWeight: "bold", fontSize: 30, fontFamily: "Segoe-UI" }}>Q 19001</Text>
                <Text style={{ fontWeight: "900", fontSize: 22, fontFamily: "Segoe-UI" }}>8:00 AM</Text>
                <Text style={{ fontWeight: "normal", fontSize: 20, fontFamily: "Segoe-UI" }}>Monday 11,May 2020</Text>
              </View>
            </ImageBackground>
          </View>
        </Pressable>
      </ScrollView>
      <TouchableHighlight onPress={() => navigation.navigate("Contacts")} style={style.callButton}>
        <View>
          <View style={style.innerCircle}>
            <Avatar size={36} source={require("./images/care.png")} activeOpacity={0.7} />
          </View>
        </View>
      </TouchableHighlight>
    </SafeAreaView>
  )
}

const style = StyleSheet.create({
  header: {
    paddingVertical: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20
  },
  card: {
    width: LAYOUT.SCREEN_WIDTH,
    height: LAYOUT.SCREEN_HEIGHT / 5,
    backgroundColor: "#FFFFFF",
    flexDirection: "column",
    borderRadius: 35
  },
  profileImage: {
    height: 50,
    width: 50,
    borderRadius: 25
  },
  innerCircle: { width: 68, height: 68, borderRadius: 68, borderColor: "#FFFFFF", borderWidth: 1, alignItems: "center", justifyContent: "center" },
  searchInputContainer: {
    height: 50,
    backgroundColor: COLORS.light,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    borderRadius: 12
  },
  sortBtn: {
    backgroundColor: COLORS.dark,
    height: 50,
    width: 50,
    borderRadius: 12,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10
  },
  optionsCard: {
    height: 238,
    width: 170,
    elevation: 15,

    //  paddingTop: 10,
    //  paddingHorizontal: 10,
    margin: 10
  },
  optionListsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
    paddingHorizontal: 20
  },
  categoryListText: {
    fontSize: 16,
    fontWeight: "bold",
    paddingBottom: 5,
    color: COLORS.grey
  },
  activeCategoryListText: {
    color: COLORS.dark,
    borderBottomWidth: 1,
    paddingBottom: 5
  },
  categoryListContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 40,
    paddingHorizontal: 40
  },
  tinyLogo: {
    width: 374,
    height: 200,
    marginHorizontal: 20,
    marginVertical: 10
  },
  cardImage: {
    width: "100%",
    height: 120,
    borderRadius: 15
  },
  loginBtn: {
    width: "80%",
    padding: 20,
    height: 70,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginLeft: 30,
    backgroundColor: "#40C381"
  },
  facility: { flexDirection: "row", marginRight: 15 },
  facilityText: { marginLeft: 5, color: COLORS.grey },
  image: {
    // resizeMode: "cover",
    width: 170,
    height: 238
  },
  imageBack: {
    // resizeMode: "cover",
    width: 165,
    height: 228,
    borderRadius: 30
  },
  callButton: {
    width: 84,
    height: 84,
    position: "absolute",
    right: 30,
    bottom: 30,
    borderRadius: 84,
    backgroundColor: "#40C381",
    alignItems: "center",
    justifyContent: "center"
  }
})
export default HomeScreen
