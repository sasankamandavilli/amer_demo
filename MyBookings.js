import "react-native-gesture-handler"
import * as React from "react"
import { View, Text, StyleSheet, SafeAreaView, ScrollView, Pressable } from "react-native"
import { Avatar } from "react-native-elements"
import * as LAYOUT from "./styles"
import Cancel from "./images/cancel.svg"
import Location from "./images/location.svg"
import Check from "./images/check.svg"

const MyBookings = ({ navigation }) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.root}>
          <View style={styles.header}>
            <Text style={{ fontFamily: "Segoe-UI", fontSize: 18, fontWeight: "900" }}>My Bookings</Text>
          </View>
          <View style={{ marginVertical: 20 }}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <View style={[styles.button, { marginLeft: 20 }]}>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 5 }}>
                  <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "900", paddingHorizontal: 4 }}>FYA</Text>
                  <View style={{ width: 24, height: 24, borderRadius: 24, backgroundColor: "#AFAFAF", justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ fontSize: 14 }}>1</Text>
                  </View>
                </View>
              </View>
              <View style={styles.upcome}>
                <View style={{ alignItems: "center", alignItems: "center", justifyContent: "center" }}>
                  <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 5 }}>
                    <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "900", paddingHorizontal: 4 }}>Upcomming</Text>
                    <View style={{ width: 24, height: 24, borderRadius: 24, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{ fontSize: 14 }}>2</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.button}>
                <View style={{ alignItems: "center", alignItems: "center", justifyContent: "center" }}>
                  <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 5 }}>
                    <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "900", paddingHorizontal: 4 }}>Drafts</Text>
                    <View style={{ width: 24, height: 24, borderRadius: 24, backgroundColor: "#AFAFAF", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{ fontSize: 14 }}>4</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.button}>
                <View style={{ alignItems: "center", alignItems: "center", justifyContent: "center" }}>
                  <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 5 }}>
                    <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "900", paddingHorizontal: 4 }}>Previous</Text>
                    <View style={{ width: 24, height: 24, borderRadius: 24, backgroundColor: "#AFAFAF", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{ fontSize: 14 }}>1</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.button}>
                <View style={{ alignItems: "center", alignItems: "center", justifyContent: "center" }}>
                  <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 5 }}>
                    <Text style={{ fontSize: 14, fontFamily: "Segoe-UI", fontWeight: "900", paddingHorizontal: 4 }}>Others</Text>
                    <View style={{ width: 24, height: 24, borderRadius: 24, backgroundColor: "#AFAFAF", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{ fontSize: 14 }}>1</Text>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
          <Pressable onPress={() => navigation.navigate("BookStack")}>
            <View style={styles.bookingCard}>
              <View style={{ width: 88, height: 23, backgroundColor: "#D9FFE7", borderRadius: 10, justifyContent: "center", alignItems: "center", margin: 20 }}>
                <Text style={{ color: "#007E2C" }}>Approved</Text>
              </View>
              <Text style={{ fontSize: 18, fontWeight: "bold", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>First Track Government Transaction</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 20, marginBottom: 10 }}>
                <View>
                  <Text style={{ fontSize: 16, fontWeight: "900", fontFamily: "Segoe-UI" }}>08:00 AM</Text>
                  <Text style={{ fontSize: 16, fontWeight: "900", fontFamily: "Segoe-UI" }}>Monday, 11 May 2020</Text>
                </View>
                <View>
                  <Text style={{ fontSize: 30, fontWeight: "900", fontFamily: "Segoe-UI" }}>Q 19001</Text>
                </View>
              </View>
              <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>Al What Street, Al Quoz 1, Behind Oasis center, ab…</Text>
              <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>+971 123456789</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-evenly", marginHorizontal: 20, marginTop: 10 }}>
                <View>
                  <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                    <Location />
                  </View>
                  <Text style={{ marginTop: 5, fontSize: 14 }}>Direction</Text>
                </View>

                <View style={{ width: 0, height: 41, backgroundColor: "#EFEFF4", borderLeftWidth: 1, borderColor: "#707070" }}></View>
                <View>
                  <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                    <Check />
                  </View>
                  <Text style={{ marginTop: 5, fontSize: 14 }}>Check</Text>
                </View>

                <View style={{ width: 0, height: 41, backgroundColor: "#EFEFF4", borderLeftWidth: 1, borderColor: "#707070" }}></View>
                <View>
                  <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                    <Cancel />
                  </View>
                  <Text style={{ marginTop: 5, fontSize: 14 }}>Cancel</Text>
                </View>
              </View>
            </View>
          </Pressable>
          <View style={styles.bookingCard}>
            <View style={{ width: 88, height: 23, backgroundColor: "#E2F4FF", borderRadius: 10, justifyContent: "center", alignItems: "center", margin: 20 }}>
              <Text style={{ color: "#0076FF" }}>Submitted</Text>
            </View>
            <Text style={{ fontSize: 18, fontWeight: "bold", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>First Track Government Transaction</Text>
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 20, marginBottom: 10 }}>
              <View>
                <Text style={{ fontSize: 16, fontWeight: "900", fontFamily: "Segoe-UI" }}>08:00 AM</Text>
                <Text style={{ fontSize: 16, fontWeight: "900", fontFamily: "Segoe-UI" }}>Monday, 11 May 2020</Text>
              </View>
              <View>
                <Text style={{ fontSize: 30, fontWeight: "900", fontFamily: "Segoe-UI" }}>Q 19001</Text>
              </View>
            </View>
            <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>Al What Street, Al Quoz 1, Behind Oasis center, ab…</Text>
            <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>+971 123456789</Text>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly", marginHorizontal: 20, marginTop: 10 }}>
              <View>
                <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                  <Location />
                </View>
                <Text style={{ marginTop: 5, fontSize: 14 }}>Direction</Text>
              </View>

              <View style={{ width: 0, height: 41, backgroundColor: "#EFEFF4", borderLeftWidth: 1, borderColor: "#707070" }}></View>
              <View>
                <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                  <Check />
                </View>
                <Text style={{ marginTop: 5, fontSize: 14 }}>Check</Text>
              </View>

              <View style={{ width: 0, height: 41, backgroundColor: "#EFEFF4", borderLeftWidth: 1, borderColor: "#707070" }}></View>
              <View>
                <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                  <Cancel />
                </View>
                <Text style={{ marginTop: 5, fontSize: 14 }}>Cancel</Text>
              </View>
            </View>
          </View>
          <View style={styles.bookingCard}>
            <View style={{ width: 88, height: 23, backgroundColor: "#D9FFE7", borderRadius: 10, justifyContent: "center", alignItems: "center", margin: 20 }}>
              <Text style={{ color: "#007E2C" }}>Approved</Text>
            </View>
            <Text style={{ fontSize: 18, fontWeight: "bold", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>First Track Government Transaction</Text>
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 20, marginBottom: 10 }}>
              <View>
                <Text style={{ fontSize: 16, fontWeight: "900", fontFamily: "Segoe-UI" }}>08:00 AM</Text>
                <Text style={{ fontSize: 16, fontWeight: "900", fontFamily: "Segoe-UI" }}>Monday, 11 May 2020</Text>
              </View>
              <View>
                <Text style={{ fontSize: 30, fontWeight: "900", fontFamily: "Segoe-UI" }}>Q 19001</Text>
              </View>
            </View>
            <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>Al What Street, Al Quoz 1, Behind Oasis center, ab…</Text>
            <Text style={{ fontSize: 14, fontWeight: "normal", fontFamily: "Segoe-UI", marginHorizontal: 20, marginBottom: 10 }}>+971 123456789</Text>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly", marginHorizontal: 20, marginTop: 10 }}>
              <View>
                <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                  <Location />
                </View>
                <Text style={{ marginTop: 5, fontSize: 14 }}>Direction</Text>
              </View>

              <View style={{ width: 0, height: 41, backgroundColor: "#EFEFF4", borderLeftWidth: 1, borderColor: "#707070" }}></View>
              <View>
                <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                  <Check />
                </View>
                <Text style={{ marginTop: 5, fontSize: 14 }}>Check</Text>
              </View>

              <View style={{ width: 0, height: 41, backgroundColor: "#EFEFF4", borderLeftWidth: 1, borderColor: "#707070" }}></View>
              <View>
                <View style={{ width: 36, height: 36, borderRadius: 35, backgroundColor: "#40C381", justifyContent: "center", alignItems: "center" }}>
                  <Cancel />
                </View>
                <Text style={{ marginTop: 5, fontSize: 14 }}>Cancel</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default MyBookings

const styles = StyleSheet.create({
  root: { backgroundColor: "#EFEFF4", width: LAYOUT.SCREEN_WIDTH },
  header: {
    backgroundColor: "#FFFFFF",
    width: LAYOUT.SCREEN_WIDTH,
    height: 83,
    borderBottomEndRadius: 20,
    borderBottomLeftRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center"
  },
  button: {
    height: 35,
    width: 95,
    borderRadius: 20,
    backgroundColor: "#FFFFFF",
    borderWidth: 1,
    borderColor: "#F5F5F5",
    marginHorizontal: 5
  },
  upcome: {
    height: 35,
    width: 120,
    borderRadius: 20,
    backgroundColor: "#FFFFFF",
    borderWidth: 1,
    borderColor: "#40C381",
    marginHorizontal: 5
  },
  bookingCard: {
    width: 374,
    height: 304,
    backgroundColor: "#FFFFFF",
    borderWidth: 1,
    borderColor: "#0000000D",
    borderRadius: 20,
    marginHorizontal: 20,
    marginBottom: 20
  }
})
