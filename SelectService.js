import React from "react"
import { View, Text, StyleSheet, TextInput, Image, SafeAreaView, ScrollView, Pressable } from "react-native"
import * as LAYOUT from "./styles"
import { Avatar } from "react-native-elements"
import Icon from "react-native-vector-icons/FontAwesome"
import Heart from "./images/heart.svg"
import Close from "./images/close.svg"
import { useState } from "react"

const SelectService = ({ sendServiceToParent }) => {
  const [select, setSelect] = useState(false)
  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
          <View style={styles.header}>
            <View></View>
            <Text style={styles.headerLabel}>Choose a Service</Text>
            <Pressable
              onPress={() => {
                setSelect(true)
                sendServiceToParent(select)
              }}
            >
              <View>
                <Close size={15} />
              </View>
            </Pressable>
          </View>
          <View style={styles.line}></View>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
            <View style={styles.searchInputContainer}>
              <Icon name="search" size={17} color={"#A7A7A7"} />
              <TextInput placeholder="Search" paddingHorizontal={10} />
            </View>
          </View>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
              <View>
                <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
              </View>
              <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                <Text style={{ fontFamily: "Roboto-Medium", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                <Text style={{ fontFamily: "Roboto", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
              </View>
            </View>
            <View>
              <Heart width={15.78} height={15.17} fill={"#40C381"} />
            </View>
          </View>
          <View style={[styles.line, { marginHorizontal: 25 }]}></View>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
              <View>
                <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
              </View>
              <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                <Text style={{ fontFamily: "Roboto-Medium", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                <Text style={{ fontFamily: "Roboto", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
              </View>
            </View>
            <View>
              <Heart width={15.78} height={15.17} fill={"#40C381"} />
            </View>
          </View>
          <View style={[styles.line, { marginHorizontal: 25 }]}></View>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
              <View>
                <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
              </View>
              <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                <Text style={{ fontFamily: "Roboto-Medium", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                <Text style={{ fontFamily: "Roboto", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
              </View>
            </View>
            <View>
              <Heart width={15.78} height={15.17} fill={"#40C381"} />
            </View>
          </View>
          <View style={[styles.line, { marginHorizontal: 25 }]}></View>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
              <View>
                <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
              </View>
              <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                <Text style={{ fontFamily: "Roboto-Medium", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                <Text style={{ fontFamily: "Roboto", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
              </View>
            </View>
            <View>
              <Heart width={15.78} height={15.17} fill={"#40C381"} />
            </View>
          </View>
          <View style={[styles.line, { marginHorizontal: 25 }]}></View>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
              <View>
                <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
              </View>
              <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                <Text style={{ fontFamily: "Roboto-Medium", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                <Text style={{ fontFamily: "Roboto", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
              </View>
            </View>
            <View>
              <Heart width={15.78} height={15.17} fill={"#40C381"} />
            </View>
          </View>
          <View style={[styles.line, { marginHorizontal: 25 }]}></View>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
              <View>
                <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
              </View>
              <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                <Text style={{ fontFamily: "Roboto-Medium", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                <Text style={{ fontFamily: "Roboto", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
              </View>
            </View>
            <View>
              <Heart width={15.78} height={15.17} fill={"#40C381"} />
            </View>
          </View>
          <View style={[styles.line, { marginHorizontal: 25 }]}></View>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
              <View>
                <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
              </View>
              <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                <Text style={{ fontFamily: "Roboto-Medium", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                <Text style={{ fontFamily: "Roboto", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
              </View>
            </View>
            <View>
              <Heart width={15.78} height={15.17} fill={"#40C381"} />
            </View>
          </View>
          <View style={[styles.line, { marginHorizontal: 25 }]}></View>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
              <View>
                <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
              </View>
              <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                <Text style={{ fontFamily: "Roboto-Medium", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                <Text style={{ fontFamily: "Roboto", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                <Text style={{ fontFamily: "Roboto", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
              </View>
            </View>
            <View>
              <Heart width={15.78} height={15.17} fill={"#40C381"} />
            </View>
          </View>
          <View style={[styles.line, { marginHorizontal: 25 }]}></View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default SelectService
const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 20,
    marginVertical: 15
  },
  headerLabel: {
    fontSize: 18,
    fontFamily: "Roboto-Medium",
    fontWeight: "700"
  },
  line: {
    borderColor: "#F0F0F0",
    borderTopWidth: 1,
    width: LAYOUT.SCREEN_WIDTH
  },
  searchInputContainer: {
    height: 36,
    backgroundColor: "#f6f6f6",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    borderRadius: 12,
    width: 374,
    marginTop: 15
  }
})
