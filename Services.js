import React from "react"
import { View, Text, StyleSheet, TextInput, Image, SafeAreaView, ScrollView, Pressable } from "react-native"
import * as LAYOUT from "./styles"
import { Avatar } from "react-native-elements"
import Icon from "react-native-vector-icons/FontAwesome"
import Heart from "./images/heart.svg"

const Centers = ({ navigation }) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.card}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 20 }}>
              <View></View>
              <View>
                <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Services</Text>
              </View>
              <View>
                <Avatar size={32} source={require("./images/face.png")} activeOpacity={0.7} />
              </View>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center", marginVertical: 15, paddingHorizontal: 20 }}>
              <View style={styles.searchInputContainer}>
                <Icon name="search" size={17} color={"#A7A7A7"} />
                <TextInput placeholder="I want" paddingHorizontal={10} />
              </View>
              <View style={{ width: 36, height: 36, borderRadius: 10, backgroundColor: "#EFEFF4", paddingLeft: 10, justifyContent: "center", padding: 10 }}>
                <Avatar size={18} source={require("./images/filter.png")} activeOpacity={0.7} />
              </View>
            </View>
          </View>
          <View style={styles.body}>
            <Pressable onPress={() => navigation.navigate("ServiceStack")}>
              <View style={[styles.serviceCard, { marginTop: 20 }]}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                  <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                    <View>
                      <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                    </View>
                    <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                      <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                      <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                      <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                    </View>
                  </View>
                  <View style={{ marginVertical: 35 }}>
                    <Heart width={15.78} height={15.17} fill={"#40C381"} />
                  </View>
                </View>
              </View>
            </Pressable>
            <View style={styles.serviceCard}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                  <View>
                    <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                  </View>
                  <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                    <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                  </View>
                </View>
                <View style={{ marginVertical: 35 }}>
                  <Heart width={15.78} height={15.17} fill={"#40C381"} />
                </View>
              </View>
            </View>
            <View style={styles.serviceCard}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                  <View>
                    <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                  </View>
                  <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                    <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                  </View>
                </View>
                <View style={{ marginVertical: 35 }}>
                  <Heart width={15.78} height={15.17} fill={"#40C381"} />
                </View>
              </View>
            </View>
            <View style={styles.serviceCard}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                  <View>
                    <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                  </View>
                  <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                    <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                  </View>
                </View>
                <View style={{ marginVertical: 35 }}>
                  <Heart width={15.78} height={15.17} fill={"#40C381"} />
                </View>
              </View>
            </View>
            <View style={styles.serviceCard}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                  <View>
                    <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                  </View>
                  <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                    <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                  </View>
                </View>
                <View style={{ marginVertical: 35 }}>
                  <Heart width={15.78} height={15.17} fill={"#40C381"} />
                </View>
              </View>
            </View>
            <View style={styles.serviceCard}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                  <View>
                    <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                  </View>
                  <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                    <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                  </View>
                </View>
                <View style={{ marginVertical: 35 }}>
                  <Heart width={15.78} height={15.17} fill={"#40C381"} />
                </View>
              </View>
            </View>
            <View style={styles.serviceCard}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                  <View>
                    <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                  </View>
                  <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                    <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                  </View>
                </View>
                <View style={{ marginVertical: 35 }}>
                  <Heart width={15.78} height={15.17} fill={"#40C381"} />
                </View>
              </View>
            </View>
            <View style={styles.serviceCard}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, marginVertical: 10 }}>
                  <View>
                    <Avatar size={51} source={require("./images/logo.png")} activeOpacity={0.7} />
                  </View>
                  <View style={{ paddingHorizontal: 10, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: "Segoe-UI", fontWeight: "900", fontSize: 18 }}>Renew Emirates ID card</Text>
                    <Text style={{ fontFamily: "Segoe-UI", color: "#40C381", fontSize: 14 }}>5 Documents required</Text>
                    <Text style={{ fontFamily: "Segoe-UI", fontSize: 16, fontWeight: "900" }}>200 AED</Text>
                  </View>
                </View>
                <View style={{ marginVertical: 35 }}>
                  <Heart width={15.78} height={15.17} fill={"#40C381"} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default Centers

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  card: {
    width: LAYOUT.SCREEN_WIDTH,
    height: 140,
    backgroundColor: "#FFFFFF",
    padding: 10,
    marginRight: 60,
    borderBottomEndRadius: 30,
    borderBottomLeftRadius: 30,
    borderColor: "red",
    flexDirection: "column"
  },
  searchInputContainer: {
    height: 36,
    backgroundColor: "#f6f6f6",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    borderRadius: 12,
    width: 326
  },
  body: {
    width: LAYOUT.SCREEN_WIDTH,
    backgroundColor: "#EFEFF4"
  },
  serviceCard: {
    width: 374,
    height: 108,
    backgroundColor: "#FFFFFF",
    borderWidth: 1,
    borderColor: "#F5F5F5",
    marginHorizontal: 20,
    marginVertical: 5,
    borderRadius: 20
  }
})
