import { StatusBar } from "expo-status-bar"
import React, { useState } from "react"
import { StyleSheet, Text, View, Image, TextInput, Button, TouchableOpacity } from "react-native"

export default function Login() {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.inputView}>
        <TextInput style={styles.TextInput} placeholder="Email." placeholderTextColor="#003f5c" onChangeText={email => setEmail(email)} />
      </View>

      <View style={styles.inputView}>
        <TextInput style={styles.TextInput} placeholder="Password." placeholderTextColor="#003f5c" secureTextEntry={true} onChangeText={password => setPassword(password)} />
      </View>

      <TouchableOpacity style={styles.loginBtn}>
        <Text style={styles.loginText}>LOGIN</Text>
      </TouchableOpacity>
      <Text>{"\n"}</Text>
      <Text>{"\n"}</Text>
      <Text>{"\n"}</Text>
      <TouchableOpacity>
        <Text style={{ color: "#FFFFFF" }}>
          Don't have an account?{" "}
          <Text style={{ color: "#FFFFFF" }} onpress={() => Linking.openURL("https://www.google.com")}>
            SignUp
          </Text>
        </Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#40C381",
    alignItems: "center",
    justifyContent: "center"
  },

  image: {
    marginBottom: 40
  },

  inputView: {
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
    color: "#FFFFFF",
    alignItems: "center"
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    color: "#FFFFFF",
    marginLeft: 20
  },

  forgot_button: {
    height: 30,
    marginBottom: 30
  },

  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#FFFFFF"
  }
})
