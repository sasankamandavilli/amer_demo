import "react-native-gesture-handler"
import * as React from "react"
import { View, Text, StyleSheet, SafeAreaView, ScrollView, Pressable } from "react-native"
import { Avatar } from "react-native-elements"
import * as LAYOUT from "./styles"
import Share from "./images/share.svg"
import Close from "./images/close.svg"

function Home({ navigation }) {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.root}>
          <View style={styles.header}>
            <View style={styles.headerRow}>
              <Pressable onPress={() => navigation.goBack()}>
                <Close fill={"#FFFFFF"} height={18} width={18} />
              </Pressable>
              <Avatar size={85} source={require("./images/logo.png")} activeOpacity={0.7} />
              <Share height={21} width={16} />
            </View>
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", marginVertical: 10 }}>
              <Text style={{ color: "#FFFFFF", fontSize: 16, fontFamily: "Roboto-Regular" }}>DNRD</Text>
            </View>
          </View>
          <View style={styles.body}>
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", paddingTop: 42 }}>
              <Text style={{ color: "#000000", fontSize: 24, fontFamily: "Roboto-Bold", fontWeight: "bold" }}>Renew Emirates ID Card</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
              <Text style={{ color: "#40C381", fontSize: 18, paddingTop: 20, fontFamily: "Roboto-Medium" }}>Fees:200AED</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", paddingHorizontal: 20 }}>
              <Text style={{ color: "#000000", fontSize: 16, paddingTop: 20, fontFamily: "Roboto-Regular", fontWeight: "normal" }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center", paddingHorizontal: 20 }}>
              <Text style={{ color: "#000000", fontSize: 18, paddingTop: 35, fontFamily: "Roboto-Bold", fontWeight: "bold" }}>Required Documents (4)</Text>
            </View>
            <View style={{ flexDirection: "row", paddingHorizontal: 20, paddingVertical: 20 }}>
              <View style={[styles.button, { width: 162 }]}>
                <Text style={styles.buttonFont}>Passport Copy</Text>
              </View>
              <View style={[styles.button, { width: 125 }]}>
                <Text style={styles.buttonFont}>Visa Copy</Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", paddingHorizontal: 15 }}>
              <View style={[styles.button, { width: 216 }]}>
                <Text style={styles.buttonFont}>Document name four</Text>
              </View>
              <View style={[styles.button, { width: 139 }]}>
                <Text style={styles.buttonFont}>Emirates ID</Text>
              </View>
            </View>
            <Pressable onPress={() => navigation.navigate("ServiceBooking")}>
              <View style={styles.startButton}>
                <Text style={{ fontFamily: "Roboto-Medium", fontSize: 20, color: "#FFFFFF", fontWeight: "700" }}>Start Service</Text>
              </View>
            </Pressable>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
export default Home

const styles = StyleSheet.create({
  root: {
    backgroundColor: "#40C381",
    flex: 1
  },
  header: {
    width: LAYOUT.SCREEN_WIDTH,
    height: 180,
    backgroundColor: "#40C381"
  },
  headerRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    paddingHorizontal: 20,
    marginTop: 30
  },
  body: {
    backgroundColor: "#FFFFFF",
    width: LAYOUT.SCREEN_WIDTH,
    borderTopEndRadius: 20,
    borderTopLeftRadius: 20
  },
  button: {
    height: 44,
    borderWidth: 1,
    borderColor: "#40C381",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 10
  },
  buttonFont: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#40C381",
    fontWeight: "800"
  },
  startButton: {
    width: 374,
    height: 61,
    marginHorizontal: 20,
    backgroundColor: "#40C381",
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 100,
    marginBottom: 30
  }
})
